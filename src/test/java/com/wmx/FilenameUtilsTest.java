package com.wmx;

import cn.hutool.core.io.resource.ResourceUtil;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

import java.io.File;
import java.net.URL;

/**
 * Apache FilenameUtils 常规文件名和文件路径操作实用程序。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/1/1 15:53
 */
public class FilenameUtilsTest {

    /**
     * String getExtension(final String filename)
     * 获取文件名的扩展名
     * 此方法返回文件名最后一个点之后的文本部分。
     * 点后不能有目录分隔符
     * * foo.txt      -> "txt"
     * * a/b/c.jpg    -> "jpg"
     * * a/b.txt/c    -> ""
     * * a/b/c        -> ""
     */
    @Test
    public void getExtension() {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getPath());

        String extension = FilenameUtils.getExtension(url.getPath());
        // json
        System.out.println(extension);
    }

    /**
     * String getBaseName(final String filename)
     * 从完整文件名中获取基名称，减去完整路径和扩展名。
     * 此方法将处理Unix或Windows格式的文件。
     * 返回最后一个正斜杠或反斜杠之后、最后一个点之前的文本。
     * * a/b/c.txt -> c
     * * a.txt     -> a
     * * a/b/c     -> c
     * * a/b/c/    -> ""
     */
    @Test
    public void getBaseName() {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getPath());

        String baseName = FilenameUtils.getBaseName(url.getPath());
        // person1
        System.out.println(baseName);
    }

    /**
     * String getFullPath(final String filename)
     * 从完整文件名（前缀+路径）中获取完整路径。
     * 此方法将处理Unix或Windows格式的文件。
     * 该方法完全基于文本，并返回最后一个前向或反斜杠之前的文本。
     * * C:\a\b\c.txt -> C:\a\b\
     * * ~/a/b/c.txt  -> ~/a/b/
     * * a.txt        -> ""
     * * a/b/c        -> a/b/
     * * a/b/c/       -> a/b/c/
     * * C:           -> C:
     * * C:\          -> C:\
     * * ~            -> ~/
     * * ~/           -> ~/
     * * ~user        -> ~user/
     * * ~user/       -> ~user/
     * <p>
     * String getFullPathNoEndSeparator(final String filename)
     * 从完整文件名中获取完整路径，该文件名是前缀+路径，也不包括最后的目录分隔符。
     * 该方法完全基于文本，并返回最后一个前向或反斜杠之前的文本。
     */
    @Test
    public void getFullPath() {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getPath());

        String fullPath = FilenameUtils.getFullPath(url.getPath());
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/
        System.out.println(fullPath);
    }

    /**
     * String getPath(final String filename)
     * 从不包含前缀的完整文件名获取路径。
     * 此方法将处理Unix或Windows格式的文件。
     * 该方法完全基于文本，并返回和之前的文本
     * 包括最后一个前向或反斜杠。
     * <pre>
     * Windows:
     * a\b\c.txt           --&gt; ""          --&gt; relative
     * \a\b\c.txt          --&gt; "\"         --&gt; current drive absolute
     * C:a\b\c.txt         --&gt; "C:"        --&gt; drive relative
     * C:\a\b\c.txt        --&gt; "C:\"       --&gt; absolute
     * \\server\a\b\c.txt  --&gt; "\\server\" --&gt; UNC
     *
     * Unix:
     * a/b/c.txt           --&gt; ""          --&gt; relative
     * /a/b/c.txt          --&gt; "/"         --&gt; absolute
     * ~/a/b/c.txt         --&gt; "~/"        --&gt; current user
     * ~                   --&gt; "~/"        --&gt; current user (slash added)
     * ~user/a/b/c.txt     --&gt; "~user/"    --&gt; named user
     * ~user               --&gt; "~user/"    --&gt; named user (slash added)
     * </pre>
     */
    @Test
    public void getPrefix() {
        File file = new File("D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json");
        System.out.println(file.getPath());
        String path = FilenameUtils.getPrefix(file.getPath());
        // D:\
        System.out.println(path);
    }

    /**
     * String getName(final String filename)
     * 从完整文件名中获取名称减去路径。
     * 此方法将处理Unix或Windows格式的文件。
     * 返回最后一个正斜杠或反斜杠之后的文本。
     * <pre>
     * a/b/c.txt -> c.txt
     * a.txt     -> a.txt
     * a/b/c     -> c
     * a/b/c/    -> ""
     * </pre>
     */
    @Test
    public void getName() {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getPath());

        String name = FilenameUtils.getName(url.getPath());
        // person1.json
        System.out.println(name);
    }
}
