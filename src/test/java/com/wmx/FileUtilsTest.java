package com.wmx;

import cn.hutool.core.io.resource.ResourceUtil;
import com.alibaba.fastjson.JSON;
import com.wmx.fastjson.Person;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.apache.commons.io.filefilter.SizeFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Apache FileUtils 通用文件操作工具类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/11/21 15:30
 */

public class FileUtilsTest {
    /**
     * write(final File file, final CharSequence data, final Charset encoding)
     * 1、file：写入的文件对象，路径不存在时，自动新建
     * 2、data：写如文件的内容
     * 3、encoding：文件内容编码
     *
     * @throws IOException
     */
    @Test
    public void write1() throws IOException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, "wmx/write1.txt");
        String context = "{\"name\":\"BeJson帅\",\"url\":\"http://www.bejson.com\"page\":88,\"isNonProfit\":true}";
        FileUtils.write(file, context, Charset.forName("UTF-8"));
        System.out.println("写入文件：" + file.getAbsoluteFile());
    }

    /**
     * write(final File file, final CharSequence data, final Charset encoding, final boolean append)
     * 1、file：写入的文件对象，路径不存在时，自动新建
     * 2、data：写如文件的内容
     * 3、encoding：文件内容编码
     * 4、append：内容是否追加
     *
     * @throws IOException
     */
    @Test
    public void write2() throws IOException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, "wmx/write2.txt");
        String context = "{\"name\":\"BeJson帅\",\"url\":\"http://www.bejson.com\"page\":88,\"isNonProfit\":true}\r\n";
        FileUtils.write(file, context, Charset.forName("UTF-8"), true);
        System.out.println("写入文件：" + file.getAbsoluteFile());
    }

    /**
     * 将字节数组写入到指定文件，路径不存在时，自动新建
     * writeByteArrayToFile(final File file, final byte[] data)
     * writeByteArrayToFile(final File file, final byte[] data, final boolean append)
     * writeByteArrayToFile(final File file, final byte[] data, final int off, final int len)
     * writeByteArrayToFile(final File file, final byte[] data, final int off, final int len,final boolean append)
     *
     * @throws IOException
     */
    @Test
    public void write3() throws IOException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, "wmx/write3.txt");
        String context = "{\"name\":\"汪茂雄\",\"url\":\"http://www.bejson.com\"page\":88,\"isNonProfit\":true}\r\n";
        FileUtils.writeByteArrayToFile(file, context.getBytes(), true);
        System.out.println("写入文件：" + file.getAbsoluteFile());
    }

    /**
     * writeLines(final File file, final Collection<?> lines)
     * writeLines(final File file, final Collection<?> lines, final boolean append)
     * writeLines(final File file, final Collection<?> lines, final String lineEnding)
     * writeLines(final File file, final Collection<?> lines, final String lineEnding,final boolean append)
     * writeLines(final File file, final String encoding, final Collection<?> lines)
     * writeLines(final File file, final String encoding, final Collection<?> lines,final boolean append)
     * writeLines(final File file, final String encoding, final Collection<?> lines,final String lineEnding)
     * writeLines(final File file, final String encoding, final Collection<?> lines,final String lineEnding, final boolean append)
     * <p>
     * file：待写入的文件，路径不存在时，自动新建；
     * lines：要写入的行，null 表示插入空行
     * append：内容是否追加
     * lineEnding：要使用的行分隔符，null 表示使用系统默认值
     * encoding：要使用的编码，{@code null}表示平台默认值
     *
     * @throws IOException
     */
    @Test
    public void write4() throws IOException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, "wmx/write4.txt");
        List<String> lines = new ArrayList<>();
        lines.add(null);
        for (int i = 0; i < 10; i++) {
            lines.add("{\"id\":" + i + ",\"name\":\"汪茂雄\",\"url\":\"http://www.bejson.com\"page\":88,\"isNonProfit\":true}");
        }
        FileUtils.writeLines(file, "UTF-8", lines, true);
        System.out.println("写入文件：" + file.getAbsoluteFile());
    }

    /**
     * writeStringToFile(final File file, final String data, final Charset encoding)
     * writeStringToFile(final File file, final String data, final Charset encoding,final boolean append)
     * writeStringToFile(final File file, final String data, final String encoding)
     * writeStringToFile(final File file, final String data, final String encoding,final boolean append)
     * <p>
     * 将字符串写入到文件
     * file：待写入的文件，路径不存在时，自动新建；
     * data：写入的字节内容；
     * append：是否追加；
     * encoding：文件内容编码
     *
     * @throws IOException
     */
    @Test
    public void write5() throws IOException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, "wmx/write5.json");

        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person(1, "张三", LocalDateTime.now(), false, 5688.99F));
        persons.add(new Person(2, "李斯", LocalDateTime.now(), false, 6688.99F));
        persons.add(new Person(3, "王五", LocalDateTime.now(), true, 8688.99F));

        String jsonString = JSON.toJSONString(persons, true);

        FileUtils.writeStringToFile(file, jsonString, "UTF-8");
        System.out.println("写入文件：" + file.getAbsoluteFile());
    }

    /**
     * long sizeOf(final File file)
     * long sizeOfDirectory(final File directory)
     * sizeOf 可以统计文件大小，也可以统计目录大小，单位为 字节，如果文件或者目录不存在，则报错
     * sizeOfDirectory 只统计目录的大小，单位为 字节，如果是文件则报错
     * <p>
     * String byteCountToDisplaySize(final long size)
     * String byteCountToDisplaySize(final BigInteger size)
     * 将文件字节大小转为可视化的 KB、MB、GB 等形式的字符串，一共有：bytes、KB、MB、GB、TB、PB、EB.
     *
     * @throws IOException
     */
    @Test
    public void sizeOf() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, "wmx/write4.txt");

        Long sizeFile = FileUtils.sizeOf(file);
        Long sizeDirectory = FileUtils.sizeOf(file.getParentFile());
        Long sizeOfDirectory = FileUtils.sizeOfDirectory(file.getParentFile());

        //2582,2661,2661
        System.out.println(sizeFile + "," + sizeDirectory + "," + sizeOfDirectory);

        String size1Show = FileUtils.byteCountToDisplaySize(sizeFile);
        String size2Show = FileUtils.byteCountToDisplaySize(sizeDirectory);
        String size3Show = FileUtils.byteCountToDisplaySize(sizeOfDirectory);

        //2 KB,2 KB,2 KB
        System.out.println(size1Show + "," + size2Show + "," + size3Show);
    }

    /**
     * copyDirectory(final File srcDir, final File destDir)
     * copyDirectory(final File srcDir, final File destDir,final boolean preserveFileDate)
     * copyDirectory(final File srcDir, final File destDir,final FileFilter filter)
     * copyDirectory(final File srcDir, final File destDir,final FileFilter filter, final boolean preserveFileDate)
     * 将指定目录下所有子孙目录和文件复制到指定的目标目录下，如果目标目录不存在，则创建该目录。如果目标目录确实存在，则此方法将源目录与目标目录合并，源目录优先。注意只能是目录，如果是文件则异常。
     *
     * @throws IOException
     */
    @Test
    public void copyDirectory() throws IOException {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getFile());
        File srdDir = new File(url.getFile()).getParentFile();

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destDir = new File(homeDirectory, srdDir.getName() + "_copy");
        FileUtils.copyDirectory(srdDir, destDir);
        System.out.println("复制目录：" + srdDir.getAbsolutePath() + " -> " + destDir.getAbsoluteFile());
    }

    /**
     * copyFile(final File srcFile, final File destFile)
     * copyFile(final File srcFile, final File destFile,final boolean preserveFileDate)
     * copyFile(final File input, final OutputStream output)
     * <p>
     * 此方法将指定源文件的内容复制到指定的目标文件，如果目标文件不存在，则会创建包含目标文件的目录，如果目标文件存在，则此方法将覆盖它。
     * srcFile：源文件，不能为 null
     * destFile：目标文件，，不能为 null
     * preserveFileDate：副本的文件日期是否与原件相同
     * output：将文件复制到字节输出流，方法在内部使用缓冲输入。
     *
     * @throws IOException
     */
    @Test
    public void copyFile() throws IOException {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getFile());
        File srcFile = new File(url.getFile());

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory, srcFile.getName());
        FileUtils.copyFile(srcFile, destFile, true);

        // 复制文件：D:\project\IDEA_project\apache-study\target\test-classes\data\person1.json -> E:\桌面\person1.json
        System.out.println("复制文件：" + srcFile.getAbsolutePath() + " -> " + destFile.getAbsoluteFile());
    }

    /**
     * copyFileToDirectory(final File srcFile, final File destDir)
     * copyFileToDirectory(final File srcFile, final File destDir, final boolean preserveFileDate)
     * 将指定源文件的内容复制到指定目标目录中同名的文件中。如果目标目录不存在，则创建该目录。如果目标文件存在，则此方法将覆盖它。
     * srcFile：源文件，不能为 null
     * destDir：目标目录，不能为 null,不存在时, 自动新建
     * preserveFileDate：副本的文件日期是否与原件相同
     *
     * @throws IOException
     */
    @Test
    public void copyFileToDirectory() throws IOException {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getFile());
        File srcFile = new File(url.getFile());

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destDir = new File(homeDirectory, srcFile.getParentFile().getName());
        FileUtils.copyFileToDirectory(srcFile, destDir, true);

        // 复制文件到指定目录：D:\project\IDEA_project\apache-study\target\test-classes\data\person1.json -> E:\桌面\data
        System.out.println("复制文件到指定目录：" + srcFile.getAbsolutePath() + " -> " + destDir.getAbsoluteFile());
    }

    /**
     * copyInputStreamToFile(final InputStream source, final File destination)
     * 将字节输入流复制到目标文件中，不存在时自动创建
     * <p>
     * copyToFile(final InputStream source, final File destination)
     * 将字节输入流复制打目标文件中，如果目标目录不存在，则将创建该目录。如果目标已存在，则将覆盖该目标。
     *
     * @throws IOException
     */
    @Test
    public void copyInputStreamToFile() throws IOException {
        String resourceName = "data/person1.json";
        InputStream inputStream = FileUtilsTest.class.getClassLoader().getResourceAsStream(resourceName);

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory, resourceName);
        FileUtils.copyInputStreamToFile(inputStream, destFile);

        // 复制文件： -> E:\桌面\data\person1.json
        System.out.println("复制文件：" + " -> " + destFile.getAbsoluteFile());
    }

    /**
     * copyToDirectory(final File src, final File destDir)
     * copyToDirectory(final Iterable<File> srcs, final File destDir)
     * 将源文件或目录及其所有内容复制到指定目标目录中同名的目录中。
     * 如果目标目录不存在，则创建该目录。如果目标目录确实存在，则此方法将源目录与目标目录合并，源目录优先。
     *
     * @throws IOException
     */
    @Test
    public void copyToDirectory() throws IOException {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getFile());
        File srdDir = new File(url.getFile()).getParentFile();

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destDir = new File(homeDirectory, srdDir.getName() + "_copy");
        FileUtils.copyToDirectory(srdDir, destDir);

        // 复制目录：D:\project\IDEA_project\apache-study\target\test-classes\data -> E:\桌面\data_copy
        System.out.println("复制目录：" + srdDir.getAbsolutePath() + " -> " + destDir.getAbsoluteFile());
    }

    /**
     * copyURLToFile(final URL source, final File destination)
     * copyURLToFile(final URL source, final File destination,final int connectionTimeout, final int readTimeout)
     * 将 URL 网络资源复制到目标文件中，可以用于下载，未设置超时时间时，可能出现永久阻塞
     * connectionTimeout：连接超时时间，单位毫秒
     * readTimeout：读取超时时间，单位毫秒
     *
     * @throws IOException
     */
    @Test
    public void copyURLToFile() throws IOException {
        URL url = new URL("https://avatar.csdnimg.cn/6/D/4/2_wangmx1993328.jpg");

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, "wmx/1.jpg");

        FileUtils.copyURLToFile(url, file, 60 * 1000, 60 * 1000);
        // 将 URL 网络资源复制到目标文件中：E:\桌面\wmx\1.jpg
        System.out.println("将 URL 网络资源复制到目标文件中：" + file.getAbsoluteFile());
    }

    /**
     * deleteDirectory(final File directory)
     * 递归删除目录。注意只能是目录，如果是文件，则异常。
     * <p>
     * deleteQuietly(final File file)
     * 安全删除文件或者递归删除目录，不会抛出任何异常。
     * <p>
     * forceDelete(final File file)	强制删除文件或者递归删除目录
     *
     * @throws IOException
     */
    @Test
    public void deleteDirectory() throws IOException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File directory = new File(homeDirectory, "wmx");
        File directory2 = new File(homeDirectory, "data_copy");

        FileUtils.deleteDirectory(directory);
        FileUtils.deleteQuietly(directory2);
        // 递归删除目录：E:\桌面\wmx
        System.out.println("递归删除目录：" + directory.getAbsoluteFile());
        // 安全删除文件或者递归删除目录：E:\桌面\data_copy
        System.out.println("安全删除文件或者递归删除目录：" + directory2.getAbsoluteFile());
    }

    /**
     * LineIterator lineIterator(final File file)
     * LineIterator lineIterator(final File file, final String encoding)
     * 为文件打开一个 InputStream 的行迭代器，完成迭代器之后，应该关闭流以释放内部资源。
     * LineIterator implements Iterator：可以很方便的一行一行读取文件内容
     *
     * @throws IOException
     */
    @Test
    public void lineIterator() throws IOException {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getFile());
        File file = new File(url.getFile());

        LineIterator lineIterator = FileUtils.lineIterator(file, "UTF-8");
        int i = 1;
        while (lineIterator.hasNext()) {
            String next = lineIterator.next();
            System.out.println((i++) + "：" + next);
        }
        //关闭底层{@code Reader}。如果只想处理较大文件的前几行，则此方法非常有用。
        // 如果不关闭迭代器，则{@code Reader}仍然存在打开。这个方法可以安全地调用多次。
        lineIterator.close();
    }

    /**
     * String readFileToString(final File file, final String encoding)
     * String readFileToString(final File file, final Charset encoding)
     * String readFileToString(final File file)
     * <p>
     * 将文件的内容读入字符串。文件始终处于关闭状态。
     * file：要读取的文件，不能是 null
     * encoding:要使用的编码，null表示使用平台默认值
     *
     * @throws IOException
     */
    @Test
    public void readFileToString() throws IOException {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getFile());
        File file = new File(url.getFile());

        String fileToString = FileUtils.readFileToString(file, Charset.forName("UTF-8"));
        System.out.println(fileToString);
    }

    /**
     * byte[] readFileToByteArray(final File file)
     * 将文件内容读入字节数组。文件始终处于关闭状态。
     *
     * @throws IOException
     */
    @Test
    public void readFileToByteArray() throws IOException {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getFile());
        File file = new File(url.getFile());

        byte[] fileToByteArray = FileUtils.readFileToByteArray(file);

        String s = new String(fileToByteArray);
        // [
        // 	{
        // 		"birthday":"2023-01-01T14:58:16.546",
        // 		"id":1,
        // 		"marry":false,
        // 		"name":"张三",
        // 		"price":5688.99
        // 	},
        // 	{
        // 		"birthday":"2023-01-01T14:58:16.546",
        // 		"id":2,
        // 		"marry":false,
        // 		"name":"李斯",
        // 		"price":6688.99
        // 	},
        // 	{
        // 		"birthday":"2023-01-01T14:58:16.546",
        // 		"id":3,
        // 		"marry":true,
        // 		"name":"王五",
        // 		"price":8688.99
        // 	}
        // ]
        System.out.println(s);
    }

    /**
     * List<String> readLines(final File file, final Charset encoding)
     * List<String> readLines(final File file, final String encoding)
     * 逐行读取文件内容到字符串列表。文件始终处于关闭状态。
     *  file：要读取的文件，不能是 null
     * encoding:要使用的编码，null表示使用平台默认值
     *
     * @throws IOException
     */
    @Test
    public void readLines() throws IOException {
        String resourceName = "data/person1.json";
        URL url = ResourceUtil.getResource(resourceName);
        // /D:/project/IDEA_project/apache-study/target/test-classes/data/person1.json
        System.out.println(url.getFile());
        File file = new File(url.getFile());

        List<String> readLines = FileUtils.readLines(file, Charset.forName("UTF-8"));
        for (String r : readLines) {
            System.out.println(r);
        }
    }

    /**
     * Collection<File> listFiles(final File directory, final String[] extensions, final boolean recursive)
     * 查找给定目录（及其子目录）中与扩展名数组匹配的文件。
     * directory 要搜索的目录
     * extensions：要过滤的扩展数组，例如{“java”、“xml”}，为 null，返回所有文件。
     * ecursive：true 表示搜索所有子目录
     */
    @Test
    public void listFiles1() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, "wmx");
        Collection<File> fileCollection = FileUtils.listFiles(file, null, true);
        for (File file1 : fileCollection) {
            System.out.println(file1.getAbsoluteFile());
        }
    }

    /**
     * 遍历目录下的所有文件
     */
    @Test
    public void listFiles2() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File targetDir = new File(homeDirectory, "wmx");
        /**
         * listFiles(final File directory, final IOFileFilter fileFilter, final IOFileFilter dirFilter)
         * directory：不要为 null、不要是文件、不要不存在
         * fileFilter：文件过滤 参数如果为 FalseFileFilter.FALSE ，则不会查询任何文件
         * dirFilter：目录过滤 参数如果为 FalseFileFilter.FALSE , 则只获取目标文件夹下的一级文件，而不会迭代获取子文件夹下的文件
         */
        Collection<File> fileCollection = FileUtils.listFiles(targetDir,
                TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        for (File file : fileCollection) {
            System.out.println(">>> " + file.getPath());
        }
    }

    /**
     * 遍历目录下所有以指定字符开头的文件
     */
    @Test
    public void listFiles3() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File targetDir = new File(homeDirectory, "wmx");
        String[] prefixArr = {"write", "2020"};//查询以这些字符开头的文件
        PrefixFileFilter prefixFileFilter = prefixArr != null ? new PrefixFileFilter(prefixArr) : new PrefixFileFilter("");
        /**
         * listFiles(final File directory, final IOFileFilter fileFilter, final IOFileFilter dirFilter)
         * directory：不要为 null、不要是文件、不要不存在
         * fileFilter：文件过滤
         *      1）PrefixFileFilter：为文件名前缀过滤器
         *      2）PrefixFileFilter 构造器参数可以是 String、List<String>、String[] 等
         *      3）如果参数为空，则表示不进行过滤，等同于 TrueFileFilter.INSTANCE
         *
         * dirFilter：目录过滤
         *      TrueFileFilter.INSTANCE：表示迭代获取所有子孙目录
         *      FalseFileFilter.FALSE：表示只获取目标目录下一级，不进行迭代
         */
        Collection<File> fileCollection = FileUtils.listFiles(targetDir, prefixFileFilter, TrueFileFilter.INSTANCE);
        for (File file : fileCollection) {
            System.out.println(">>> " + file.getPath());
        }
    }

    /**
     * 遍历目录下所有以指定字符结尾的文件
     */
    @Test
    public void listFiles4() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File targetDir = new File(homeDirectory, "wmx");
        String[] suffixArr = {".txt", ".jpg"};//查询以这些字符结尾的文件

        /**
         * SuffixFileFilter 指定的后缀可以是任意字符，如文件 overview-tree.html，后缀过滤器字符 html、.html、tree.html 等都能匹配
         * */
        SuffixFileFilter suffixFileFilter = suffixArr != null ? new SuffixFileFilter(suffixArr) : new SuffixFileFilter("");
        Collection<File> fileCollection = FileUtils.listFiles(targetDir, suffixFileFilter, TrueFileFilter.INSTANCE);
        for (File file : fileCollection) {
            System.out.println(">>> " + file.getPath());
        }
    }

    /**
     * 遍历目录下所有大于或者小于指定大小的文件
     */
    @Test
    public void listFiles5() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File targetDir = new File(homeDirectory, "wmx");
        Long fileSize = FileUtils.ONE_MB;//文件判断大小，单位 字节
        boolean acceptLarger = false;//为 true，则表示获取大于等于 fileSize 的文件,false 相反

        /**
         * SizeFileFilter(final long size, final boolean acceptLarger)
         * 1）文件大小过滤器
         * 2）size，表示判断的依据点
         * 3）acceptLarger：为 true，则表示获取  大于等于 size 的文件为 false ，则表示获取 小于 size 的文件
         * 4）单位是字节
         */
        SizeFileFilter sizeFileFilter = new SizeFileFilter(fileSize, acceptLarger);
        Collection<File> fileCollection = FileUtils.listFiles(targetDir, sizeFileFilter, TrueFileFilter.INSTANCE);
        for (File file : fileCollection) {
            System.out.println(">>> " + file.getPath() + "\t" + file.length() +
                    "\t" + FileUtils.byteCountToDisplaySize(file.length()));
        }
    }

    //获取发版路径下的脚本
    @Test
    public void getScriptPaths() {
        File file = new File("E:\\wmx\\基础库发版\\20210629\\脚本");
        Collection<File> fileCollection = FileUtils.listFiles(file, null, true);

        String root = file.getAbsoluteFile().toString() + "\\";
        int i = 0;
        for (File file1 : fileCollection) {
            System.out.println((++i) + " ==== " + file1.getAbsoluteFile().toString().replace(root, ""));
        }
    }
}