package com.wmx.spring;

import org.junit.Test;
import org.springframework.aop.support.AopUtils;
import org.springframework.util.ClassUtils;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * org.springframework.aop.support.AopUtils - 提供=面向切面编程相关的工具方法
 * 主要方法：
 * isAopProxy(Object obj)：检查给定对象是否为AOP代理。
 * isCglibProxy(@Nullable Object object)：检查给定的对象是否是CGLIB代理。
 * isJdkDynamicProxy(@Nullable Object object)：检查给定的对象是否是JDK动态代理。
 * isFinalizeMethod(@Nullable Method method)： 确定给定的方法是否为“finalize”方法。
 * isHashCodeMethod(@Nullable Method method)： 确定给定的方法是否是“hashCode”方法。
 * isToStringMethod(@Nullable Method method)： 确定给定的方法是否是“toString”方法。
 * getTargetClass(Object candidate)：返回AOP代理的目标类，否则返回普通类
 * invokeJoinpointUsingReflection(@Nullable Object target, Method method, Object[] args): 作为AOP方法调用的一部分，通过反射调用给定的目标。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/7/7 10:44
 */
public class AopUtilsTest {

    /**
     * 通过反射调用给定的目标
     *
     * @throws Throwable
     */
    @Test
    public void testInvokeJoinpointUsingReflection() throws Throwable {
        Object object = ClassUtils.getUserClass(HashMap.class).newInstance();
        Method putMethod = ClassUtils.getMethod(HashMap.class, "put", Object.class, Object.class);
        Method getMethod = ClassUtils.getMethod(HashMap.class, "get", Object.class);

        Object[] putArgs = {"code", "001001"};
        Object[] getArgs = {"code"};
        AopUtils.invokeJoinpointUsingReflection(object, putMethod, putArgs);
        Object value = AopUtils.invokeJoinpointUsingReflection(object, getMethod, getArgs);

        // {code=001001}
        System.out.println(object);
        // 001001
        System.out.println(value);
    }
}
