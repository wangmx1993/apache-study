package com.wmx.spring;

import java.util.Date;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/12/7 19:04
 */
public class Book {

    private String bid;
    private String title;
    private Float price;
    private Date publishDate;
    private Boolean isShow;
    private Integer stock;

    public Book() {
    }

    public Book(String bid, String title, Float price, Date publishDate, Boolean isShow, Integer stock) {
        this.bid = bid;
        this.title = title;
        this.price = price;
        this.publishDate = publishDate;
        this.isShow = isShow;
        this.stock = stock;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public Boolean getShow() {
        return isShow;
    }

    public void setShow(Boolean show) {
        isShow = show;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bid='" + bid + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", publishDate=" + publishDate +
                ", isShow=" + isShow +
                ", stock=" + stock +
                '}';
    }
}
