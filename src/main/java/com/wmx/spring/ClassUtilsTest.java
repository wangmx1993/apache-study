package com.wmx.spring;

import org.junit.Test;
import org.springframework.util.ClassUtils;

/**
 * org.springframework.util.ClassUtils - 提供类和类加载器相关的工具方法。
 * 1、获取接口、类、类加载器、方法，判断类、对象是否 CGLIB 代理、判断方法是否是重写的方法。
 * 2、判断类是否是数组，
 * 主要方法：
 * String convertClassNameToResourcePath(String className): 将基于“.”的完全限定类名转换为基于“/”的资源路径。
 * String convertResourcePathToClassName(String resourcePath): 将基于“/”的资源路径转换为基于“.”的完全限定类名。
 * <p>
 * String getClassFileName(Class<?> clazz)： 确定类文件的名称，相对于包含的包：例如“String.class”
 * Constructor<T> getConstructorIfAvailable(Class<T> clazz, Class<?>... paramTypes): 确定给定类是否有具有给定签名的公共构造函数，如果可用，则返回该构造函数（否则返回｛@code null｝）。
 * ClassLoader getDefaultClassLoader()：返回要使用的默认ClassLoader：如果可用，通常是线程上下文ClassLoader；加载ClassUtils类的ClassLoader将用作回退。
 * Method getMethod(Class<?> clazz, String methodName, @Nullable Class<?>... paramTypes)：确定给定类是否具有给定签名的公共方法，并在可用时返回该方法（否则抛出｛@code IllegalStateException｝）。
 * String getPackageName(Class<?> clazz)：确定给定类的包的名称，例如{@code java.lang.String}类的“java.lang”。
 * Method getStaticMethod(Class<?> clazz, String methodName, Class<?>... args)：返回类的公共静态方法。如果没有找到静态方法，返回null。
 * String getShortNameAsProperty(Class<?> clazz): 以非大写JavaBeans属性格式返回Java类的短字符串名称。如果是内部类，则剥离外部类名。
 * String getShortName(Class<?> clazz) :获取没有限定包名的类名。
 * <p>
 * boolean isAssignableValue(Class<?> type, @Nullable Object value)：假设通过反射进行设置，确定给定类型是否可从给定值赋值。将基元包装类视为可分配给相应的基元类型。
 * boolean isCglibProxyClassName(@Nullable String className)：检查指定的类名是否是CGLIB生成的类。
 * boolean isCglibProxyClass(@Nullable Class<?> clazz)：检查指定的类是否是CGLIB生成的类。
 * boolean isCglibProxy(Object object)：检查给定的对象是否是CGLIB代理。
 * boolean isInnerClass(Class<?> clazz)：确定提供的类是否是<em>内部类</em>，即封闭类的非静态成员。
 * <p>
 * boolean hasAtLeastOneMethodWithName(Class<?> clazz, String methodName)：给定的类或它的一个超类是否至少有一个或多个方法具有所提供的名称（具有任何参数类型）？包括非公共方法。
 * boolean hasConstructor(Class<?> clazz, Class<?>... paramTypes)：确定给定类是否具有具有给定签名的公共构造函数。
 * * 本质上将{@code NoSuchMethodException}转换为“false”。
 * boolean hasMethod(Class<?> clazz, String methodName, Class<?>... paramTypes)：确定给定类是否具有具有给定签名的公共方法。
 * * 本质上将{@code NoSuchMethodException}转换为“false”。
 * <p>
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/7/7 10:27
 */
public class ClassUtilsTest {

    @Test
    public void testGetXxx() {
        String classFileName = ClassUtils.getClassFileName(ClassUtilsTest.class);
        ClassLoader defaultClassLoader = ClassUtils.getDefaultClassLoader();
        String packageName = ClassUtils.getPackageName(ClassUtilsTest.class);
        String shortName = ClassUtils.getShortName(ClassUtilsTest.class);

        // ClassUtilsTest.class
        System.out.println(classFileName);
        // sun.misc.Launcher$AppClassLoader@18b4aac2
        System.out.println(defaultClassLoader);
        // com.wmx.spring
        System.out.println(packageName);
        // ClassUtilsTest
        System.out.println(shortName);
    }

}
