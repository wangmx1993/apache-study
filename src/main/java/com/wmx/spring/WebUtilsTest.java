package com.wmx.spring;

import com.wmx.pojo.ResultData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * 类路径：org.springframework.web.util.WebUtils - Web应用程序提供工具方法。
 * 主要方法：
 * getRealPath(ServletContext servletContext, String path)：获取相对于Web应用根目录的真实路径。
 * Cookie getCookie(HttpServletRequest request, String name)： 检索具有给定名称的第一个cookie。请注意，多个cookie可以具有相同的名称，但路径或域不同。
 * Object getRequiredSessionAttribute(HttpServletRequest request, String name)：从请求中获取Session属性值，没有时将抛出异常。
 * Object getSessionAttribute(HttpServletRequest request, String name)：从请求中获取Session属性值，没有时返回null。
 * String getSessionId(HttpServletRequest request)： 确定给定请求的会话id（如果有的话）。 没有时返回null。
 * File getTempDir(ServletContext servletContext)：返回当前web应用程序的临时目录，
 * <p>
 * findParameterValue(Map<String, ?> params, String paramName)：在参数Map中查找指定的参数值。
 * findParameterValue(ServletRequest request, String name): 从给定的请求参数中获取命名参数。
 * <p>
 * boolean isValidOrigin(HttpRequest request, Collection<String> allowedOrigins): 根据允许的来源列表检查给定的请求来源。
 * * 包含“*”的列表表示允许所有来源。空列表表示只允许使用相同的原点。
 * <p>
 * setSessionAttribute(HttpServletRequest request, String name, @Nullable Object value) :将具有给定名称的会话属性设置为给定值。
 * * 如果值为null（如果存在会话），则删除会话属性。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/7/7 11:08
 */
@RestController
public class WebUtilsTest {

    /**
     * http://localhost:8080/webUtil/getRealPath?code=110蚩尤后裔
     *
     * @param request
     * @return
     * @throws FileNotFoundException
     */
    @GetMapping("webUtil/getRealPath")
    public ResultData getRealPath(HttpServletRequest request) throws FileNotFoundException {
        String realPath = WebUtils.getRealPath(request.getServletContext(), "/");
        File tempDir = WebUtils.getTempDir(request.getServletContext());
        String sessionId = WebUtils.getSessionId(request);
        String code = WebUtils.findParameterValue(request, "code");

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("realPath", realPath);
        dataMap.put("tempDir", tempDir);
        dataMap.put("sessionId", sessionId);
        dataMap.put("code", code);

        // {
        //     "code": "110蚩尤后裔",
        //     "tempDir": "C:\\Users\\A\\AppData\\Local\\Temp\\tomcat.326330186098690636.8080\\work\\Tomcat\\localhost\\ROOT",
        //     "realPath": "C:\\Users\\A\\AppData\\Local\\Temp\\tomcat-docbase.1481660012124617897.8080\\",
        //     "sessionId": null
        //   }
        return ResultData.success(dataMap);
    }

}
