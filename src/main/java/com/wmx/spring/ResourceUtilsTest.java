package com.wmx.spring;

import org.junit.Test;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * ResourceUtils 资源文件工具类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/2/21 19:54
 */
@RestController
public class ResourceUtilsTest {

    /**
     * 判断字符串是否是一个合法的 URL 字符串: boolean isUrl(String resourceLocation)
     * 确定给定的URL是否指向文件系统中的资源: boolean isFileURL(URL url)
     * 确定给定的URL是否指向jar文件本身，即是否具有协议“file”，并以“.jar”扩展名结尾: boolean isJarFileURL(URL url)
     * 确定给定的URL是否指向jar文件中的资源。具有协议“jar”、“war”、“zip”、“vfszip”或“wsjar”: boolean isJarURL(URL url)
     */
    @Test
    public void testIsUrl() throws MalformedURLException, FileNotFoundException {
        URL resource = ResourceUtilsTest.class.getClassLoader().getResource("application.properties");
        //file:/D:/project/IDEA_project/apache-study/target/classes/application.properties
        System.out.println(resource.toString());

        //true
        System.out.println(ResourceUtils.isUrl(resource.toString()));
        //true
        System.out.println(ResourceUtils.isFileURL(new URL(resource.toString())));
        //false
        System.out.println(ResourceUtils.isJarFileURL(new URL(resource.toString())));
        //false
        System.out.println(ResourceUtils.isJarURL(new URL(resource.toString())));

        System.out.println("----");
        //D:\project\IDEA_project\apache-study\target\classes\application.properties
        System.out.println(ResourceUtils.getFile(resource.toString()));
        //false
        System.out.println(ResourceUtils.isUrl(ResourceUtils.getFile(resource.toString()).toString()));
        //true
        System.out.println(ResourceUtils.isFileURL(ResourceUtils.getFile(resource.toString()).toURL()));
    }

    /**
     * 获取 URL：static URL getURL(String resourceLocation)
     * 获取文件（JAR 包内部的文件无法获取，必须是一个独立的文件）：
     * File getFile(String resourceLocation)
     * File getFile(URI resourceUri)
     * File getFile(URL resourceUrl)
     */
    @Test
    public void testGetUrl() throws IOException, URISyntaxException {
        URL resource = ResourceUtilsTest.class.getClassLoader().getResource("logback.xml");
        //file:/D:/project/IDEA_project/apache-study/target/classes/application.properties
        System.out.println(resource.toString());

        URL url = ResourceUtils.getURL(resource.toString());
        //4727
        System.out.println(url.openConnection().getContentLength());

        File file = new File(System.getProperty("user.dir"), "pom.xml");
        File getFile = ResourceUtils.getFile(file.toString());
        //D:\project\IDEA_project\apache-study\pom.xml
        System.out.println(getFile.getAbsoluteFile());

        URI uri = ResourceUtils.toURI(url);
        //file:/D:/project/IDEA_project/apache-study/target/classes/logback.xml
        System.out.println(uri.toString());
    }

}
