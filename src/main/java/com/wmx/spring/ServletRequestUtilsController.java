package com.wmx.spring;

import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Springframework web Servlet 请求参数工具类 ServletRequestUtils
 * 1、ServletRequestUtils 用于获取 http 请求地址上的查询参数，并转为需要的数据类型，默认 HttpServletRequest 获取的参数值都是字符串类型。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/12/10 10:05
 */
@RestController
public class ServletRequestUtilsController {

    /**
     * 原生 HttpServletRequest 获取查询参数
     * http://localhost:8080/servlet/getParameters/1993?token=2f2ef70923323&year=2022&price=105.89&isShow=true
     */
    @GetMapping("servlet/getParameters/1993")
    @SuppressWarnings("all")
    public Map<String, Object> testGetParameters(HttpServletRequest request) {
        Map<String, Object> dataMap = new HashMap<>(8);
        String token = request.getParameter("token");
        String year = request.getParameter("year");
        String price = request.getParameter("price");
        String isShow = request.getParameter("isShow");

        dataMap.put("token", token);
        dataMap.put("year", year);
        dataMap.put("price", price);
        dataMap.put("isShow", isShow);
        //{"year":"2022","price":"105.89","token":"2f2ef70923323","isShow":"true"}
        return dataMap;
    }

    /**
     * springframework web {@link ServletRequestUtils} 获取查询参数值
     * http://localhost:8080/servlet/getParameters2/1993?token=2f2ef70923323&year=2022&price=105.89&isShow=true
     * <p>
     * Boolean getBooleanParameter(ServletRequest request, String name)
     * Double getDoubleParameter(ServletRequest request, String name)
     * Float getFloatParameter(ServletRequest request, String name)
     * Integer getIntParameter(ServletRequest request, String name)
     * Long getLongParameter(ServletRequest request, String name)
     * String getStringParameter(ServletRequest request, String name)
     * <p>
     * 1、name 参数名称不存在时，默认返回 null。
     * 2、name 参数名称存在，但是值无法解析为 double、flout、int、long 时，则报错 NumberFormatException(为空也会解析报错).
     * 3、解析 boolean 值时，true、yes、1 都会作为 true，其它任意字符都会解析为 boolean 值的 false。
     *
     * @param request
     * @return
     */
    @GetMapping("servlet/getParameters2/1993")
    @SuppressWarnings("all")
    public Map<String, Object> testGetParameters2(HttpServletRequest request) {
        Map<String, Object> dataMap = new HashMap<>(8);
        try {
            String token = ServletRequestUtils.getStringParameter(request, "token");
            Boolean isShow = ServletRequestUtils.getBooleanParameter(request, "isShow");
            Long year = ServletRequestUtils.getLongParameter(request, "year");
            Double price = ServletRequestUtils.getDoubleParameter(request, "price");

            dataMap.put("seeionId", request.getSession().getId());
            dataMap.put("token", token);
            dataMap.put("year", year);
            dataMap.put("price", price);
            dataMap.put("isShow", isShow);
        } catch (ServletRequestBindingException e) {
            e.printStackTrace();
        }
        //{"year":2022,"price":105.89,"token":"2f2ef70923323","isShow":true}
        return dataMap;
    }

    /**
     * http://localhost:8080/servlet/getParameters2/1993?token=2f2ef70923323&year=2022&price=105.89&isShow=true
     * http://localhost:8080/servlet/getParameters3/1993
     * <p>
     * Boolean getBooleanParameter(ServletRequest request, String name, boolean defaultVal)
     * Double getDoubleParameter(ServletRequest request, String name, double defaultVal)
     * Float getFloatParameter(ServletRequest request, String name, float defaultVal)
     * Integer getIntParameter(ServletRequest request, String name, int defaultVal)
     * Long getLongParameter(ServletRequest request, String name, long defaultVal)
     * String getStringParameter(ServletRequest request, String name, String defaultVal)
     * <p>
     * 1、name 参数名称不存在时，默认返回 null。
     * 2、name 参数名称存在，但是值无法解析为 double、flout、int、long 时，则报错 NumberFormatException(为空也会解析报错).
     * 3、解析 boolean 值时，true、yes、1 都会作为 true，其它任意字符都会解析为 boolean 值的 false。
     * 4、当参数不存在时，则返回默认值 defaultVal
     *
     * @param request
     * @return
     */
    @GetMapping("servlet/getParameters3/1993")
    @SuppressWarnings("all")
    public Map<String, Object> testGetParameters3(HttpServletRequest request) {
        Map<String, Object> dataMap = new HashMap<>(8);
        String token = ServletRequestUtils.getStringParameter(request, "token", "0");
        Boolean isShow = ServletRequestUtils.getBooleanParameter(request, "isShow", false);
        Long year = ServletRequestUtils.getLongParameter(request, "year", 0);
        Double price = ServletRequestUtils.getDoubleParameter(request, "price", 0.0);

        dataMap.put("token", token);
        dataMap.put("year", year);
        dataMap.put("price", price);
        dataMap.put("isShow", isShow);
        //{"year":0,"price":0.0,"token":"0","isShow":false}
        return dataMap;
    }

    /**
     * http://localhost:8080/servlet/getParameters4/1993?token=2f2ef70923323&year=2022&price=105.89&isShow=true
     * 如果找不到该参数, 即参数不存在时，则引发异常
     * <p>
     * Boolean getBooleanParameter(ServletRequest request, String name)
     * Double getDoubleParameter(ServletRequest request, String name)
     * Float getFloatParameter(ServletRequest request, String name)
     * Integer getIntParameter(ServletRequest request, String name)
     * Long getLongParameter(ServletRequest request, String name)
     * String getStringParameter(ServletRequest request, String name)
     * <p>
     *
     * @param request
     * @return
     */
    @GetMapping("servlet/getParameters4/1993")
    @SuppressWarnings("all")
    public Map<String, Object> testGetParameters4(HttpServletRequest request) {
        Map<String, Object> dataMap = new HashMap<>(8);
        try {
            String token = ServletRequestUtils.getRequiredStringParameter(request, "token");
            Boolean isShow = ServletRequestUtils.getRequiredBooleanParameter(request, "isShow");
            Long year = ServletRequestUtils.getRequiredLongParameter(request, "year");
            Double price = ServletRequestUtils.getRequiredDoubleParameter(request, "price");

            dataMap.put("token", token);
            dataMap.put("year", year);
            dataMap.put("price", price);
            dataMap.put("isShow", isShow);
        } catch (ServletRequestBindingException e) {
            e.printStackTrace();
        }
        return dataMap;
    }

    /**
     * http://localhost:8080/servlet/getParameters5/1993?token=t189P&token=t284P&&year=2022&year=2023&price=105.89&price=255.66&isShow=true&isShow=false
     * http://localhost:8080/servlet/getParameters5/1993
     * 获取参数值数组，如果未找到，则返回空数组，当查询参数出现多个同名参数时，接收的值就是数组
     * <p>
     * boolean[] getBooleanParameters(ServletRequest request, String name)
     * double[] getDoubleParameters(ServletRequest request, String name)
     * float[] getFloatParameters(ServletRequest request, String name)
     * int[] getIntParameters(ServletRequest request, String name)
     * long[] getLongParameters(ServletRequest request, String name)
     * String[] getStringParameters(ServletRequest request, String name)
     * <p>
     *
     * @param request
     * @return
     */
    @GetMapping("servlet/getParameters5/1993")
    @SuppressWarnings("all")
    public Map<String, Object> testGetParameters5(HttpServletRequest request) {
        Map<String, Object> dataMap = new HashMap<>(8);
        try {
            String[] tokens = ServletRequestUtils.getStringParameters(request, "token");
            boolean[] isShows = ServletRequestUtils.getBooleanParameters(request, "isShow");
            long[] years = ServletRequestUtils.getLongParameters(request, "year");
            double[] prices = ServletRequestUtils.getDoubleParameters(request, "price");

            dataMap.put("token", tokens);
            dataMap.put("year", years);
            dataMap.put("price", prices);
            dataMap.put("isShow", isShows);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //{"year":[2022,2023],"price":[105.89,255.66],"token":["t189P","t284P"],"isShow":[true,false]}
        //{"year":[],"price":[105.89,255.66],"token":["t189P"],"isShow":[true,false]}
        //{"year":[],"price":[],"token":[],"isShow":[]}
        return dataMap;
    }


}
