package com.wmx.spring;

import org.junit.Before;
import org.junit.Test;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * springframework 文件、资源、IO 流工具类 FileCopyUtils
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/1/14 16:36
 */
public class FileCopyUtilsTest {

    private String userDir = "";

    @Before
    public void before() {
        userDir = System.getProperty("user.dir");
        System.out.println("用户的当前工作目录:" + userDir);

    }

    /**
     * byte[] copyToByteArray(File in) 将给定输入文件的内容复制到新的字节数组中。
     * byte[] copyToByteArray(@Nullable InputStream in) 将给定InputStream的内容复制到新的字节数组中,完成后自动关闭流，
     *
     * @throws IOException
     */
    @Test
    public void testCopyToByteArray() throws IOException {
        File file = new File(userDir, "pom.xml");

        byte[] bytes = FileCopyUtils.copyToByteArray(file);
        System.out.println(new String(bytes));
    }

    /**
     * String copyToString(@Nullable Reader in) 将给定 in 的内容复制到字符串中。完成后关闭 in。
     *
     * @throws IOException
     */
    @Test
    public void testCopyToString() throws IOException {
        File file = new File(userDir, "pom.xml");

        String copyToString = FileCopyUtils.copyToString(new FileReader(file));
        System.out.println(copyToString);
    }

    /**
     * void copy(byte[] in, File out): 将给定字节数组的内容复制到给定的输出文件，完成后自动关流。
     * void copy(byte[] in, OutputStream out): 将给定字节数组的内容复制到给定的输出流，完成后自动关流。
     * int copy(File in, File out) 将给定输入文件的内容复制到给定输出文件，完成后自动关流。返回复制的字节数。
     * int copy(InputStream in, OutputStream out)将给定InputStream的内容复制到给定OutputStream。完成后关闭两个流。
     * int copy(Reader in, Writer out)：文件复制，自动关流
     * void copy(String in, Writer out)：文件复制，自动关流
     *
     * @throws IOException
     */
    @Test
    public void testCopy() throws IOException {
        File destFile = new File(userDir, System.currentTimeMillis() + ".txt");
        String message = "蚩尤后裔";
        FileCopyUtils.copy(message.getBytes(), destFile);

        System.out.println("写入文件：" + destFile);
    }


}
