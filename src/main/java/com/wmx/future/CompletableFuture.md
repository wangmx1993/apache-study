
# CompletableFuture 并发工具常用方法

  
## 串行执行任务，Lambda 函数无参数，无返回值。带 Async 的方法是支持用不同的线程执行任务

    CompletableFuture<Void> thenRun(Runnable action)
    CompletableFuture<Void> thenRunAsync(Runnable action)
    CompletableFuture<Void> thenRunAsync(Runnable action, Executor executor)

## 串行执行任务，Lambda 函数有参数，有返回值
    
    <U> CompletableFuture<U> thenApply(Function<? super T,? extends U> fn)
    <U> CompletableFuture<U> thenApplyAsync(Function<? super T,? extends U> fn)
    <U> CompletableFuture<U> thenApplyAsync(Function<? super T,? extends U> fn, Executor executor)

## 串行执行任务，Lambda 函数有参数，无返回值
    
    CompletableFuture<Void> thenAccept(Consumer<? super T> action)
    CompletableFuture<Void> thenAcceptAsync(Consumer<? super T> action)
    CompletableFuture<Void> thenAcceptAsync(Consumer<? super T> action, Executor executor)

## 类似 thenApply，当 thenCompose 更适合 CompletableFuture 嵌套的情况，即 CompletableFuture 执行的任务里面又有 CompletableFuture 在执行任务
    
    <U> CompletableFuture<U> thenCompose(Function<? super T, ? extends CompletionStage<U>> fn)  
    <U> CompletableFuture<U> thenComposeAsync(Function<? super T, ? extends CompletionStage<U>> fn)
    <U> CompletableFuture<U> thenComposeAsync(Function<? super T, ? extends CompletionStage<U>> fn, Executor executor)

## 聚合两个独立 Future 的结果
    
    <U,V> CompletableFuture<V> thenCombine(CompletionStage<? extends U> other, BiFunction<? super T,? super U,? extends V> fn)
    <U,V> CompletableFuture<V> thenCombineAsync(CompletionStage<? extends U> other, BiFunction<? super T,? super U,? extends V> fn)
    <U,V> CompletableFuture<V> thenCombineAsync(CompletionStage<? extends U> other, BiFunction<? super T,? super U,? extends V> fn, Executor executor)
    <U> CompletableFuture<Void> thenAcceptBoth(CompletionStage<? extends U> other, BiConsumer<? super T, ? super U> action)
    <U> CompletableFuture<Void> thenAcceptBothAsync(CompletionStage<? extends U> other, BiConsumer<? super T, ? super U> action)
    <U> CompletableFuture<Void> thenAcceptBothAsync( CompletionStage<? extends U> other, BiConsumer<? super T, ? super U> action, Executor executor)
    CompletableFuture<Void> runAfterBoth(CompletionStage<?> other, Runnable action)
    CompletableFuture<Void> runAfterBothAsync(CompletionStage<?> other, Runnable action)
    CompletableFuture<Void> runAfterBothAsync(CompletionStage<?> other, Runnable action, Executor executor)

## 聚合 Or 关系 Either...or... 表示两者中的一个，自然也就是 Or 的体现了

    <U> CompletableFuture<U> applyToEither(CompletionStage<? extends T> other, Function<? super T, U> fn)
    <U> CompletableFuture<U> applyToEitherAsync(、CompletionStage<? extends T> other, Function<? super T, U> fn)
    <U> CompletableFuture<U> applyToEitherAsync(CompletionStage<? extends T> other, Function<? super T, U> fn, Executor executor)
    
    CompletableFuture<Void> acceptEither(CompletionStage<? extends T> other, Consumer<? super T> action)
    CompletableFuture<Void> acceptEitherAsync(CompletionStage<? extends T> other, Consumer<? super T> action)
    CompletableFuture<Void> acceptEitherAsync(CompletionStage<? extends T> other, Consumer<? super T> action, Executor executor)
    CompletableFuture<Void> runAfterEither(CompletionStage<?> other, Runnable action)
    CompletableFuture<Void> runAfterEitherAsync(CompletionStage<?> other, Runnable action)
    CompletableFuture<Void> runAfterEitherAsync(CompletionStage<?> other, Runnable action, Executor executor)

## 异常处理：exceptionally 相当于 try-catch-finally 中的 catch；handle 相当于 finally

    CompletableFuture<T> exceptionally(Function<Throwable, ? extends T> fn)
    CompletableFuture<T> exceptionallyAsync(Function<Throwable, ? extends T> fn)
    CompletableFuture<T> exceptionallyAsync(Function<Throwable, ? extends T> fn, Executor executor)
    CompletableFuture<T> whenComplete(BiConsumer<? super T, ? super Throwable> action)
    CompletableFuture<T> whenCompleteAsync(BiConsumer<? super T, ? super Throwable> action)
    CompletableFuture<T> whenCompleteAsync(BiConsumer<? super T, ? super Throwable> action, Executor executor)
    <U> CompletableFuture<U> handle(BiFunction<? super T, Throwable, ? extends U> fn)
    <U> CompletableFuture<U> handleAsync(BiFunction<? super T, Throwable, ? extends U> fn)
    <U> CompletableFuture<U> handleAsync(BiFunction<? super T, Throwable, ? extends U> fn, Executor executor)


