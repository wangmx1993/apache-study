package com.wmx.apachestudy.httpclient;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;

/**
 * Apache client put 请求
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/8/1 10:29
 */
public class HttpPutTest {

    /**
     * htp put 请求与 post 请求操作步骤是一样的，完全类似 post.
     */
    @Test
    public void testPut1() {
        String url = "http://localhost:8080/example/httpPut?info=put请求";
        CloseableHttpClient httpClient = null;
        HttpPut httpPut = null;
        CloseableHttpResponse httpResponse = null;
        try {
            //1、创建 CloseableHttpClient 同步请求对象
            httpClient = HttpClientBuilder.create().build();

            // 2、HttpPost(final String uri)：创建 http post 请求对象, 设置请求头信息
            httpPut = new HttpPut(url);
            httpPut.setHeader("Content-Type", "application/json;charset=utf8");

            // 3、设置 post 请求正文参数
            String postParams = "{\"adReqData\":{\"chid\":9,\"adtype\":0,\"uin\":null,\"qq_openid\":\"\",\"slot\":[{\"loid\":\"1\",\"cur\":0,\"channel\":\"world\",\"orders_info\":[],\"article_id\":\"\",\"refresh_type\":1,\"current_rot\":\"\",\"seq\":\"\",\"seq_loid\":\"\"}],\"netstatus\":\"unknown\",\"pf\":\"pc\",\"appversion\":\"200717\",\"plugin_news_cnt\":20,\"plugin_page_type\":\"\",\"plugin_text_ad\":false,\"plugin_bucket_id\":\"\"}}";
            httpPut.setEntity(new StringEntity(postParams, "UTF-8"));

            // 4、CloseableHttpResponse execute(final HttpUriRequest request)：执行请求
            // 如果连接不上服务器，则抛出:java.net.ConnectException: Connection refused: connect
            httpResponse = httpClient.execute(httpPut);

            // 5、获取响应结果, 状态码 200 表示请求成功
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            System.out.println("响应状态码：" + statusCode);
            if (statusCode == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                // 使用指定的字符编码解析响应消息实体
                String feedback = EntityUtils.toString(httpEntity, "UTF-8");
                System.out.println("feedback：" + feedback);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            /**
             * consume(final HttpEntity entity)：确保实体内容已完全使用，并且内容流（如果存在）已关闭。
             *  httpResponse.close()：关闭此流并释放与之关联的所有系统资源。如果流已关闭，则调用此方法无效。
             */
            try {
                if (httpResponse != null) {
                    EntityUtils.consume(httpResponse.getEntity());
                    httpResponse.close();
                }
                //无论成功与否,都要释放连接并终止
                if (httpPut != null && !httpPut.isAborted()) {
                    httpPut.releaseConnection();
                    httpPut.abort();
                }
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
