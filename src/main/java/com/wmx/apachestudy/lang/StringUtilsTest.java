package com.wmx.apachestudy.lang;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 字符串工具类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/6/12 15:34
 */
@SuppressWarnings("all")
public class StringUtilsTest {

    /**
     * void testSubstring()：从指定的字符串中获取子字符串以避免异常,负数可用于从字符串结尾开始/结束
     * * <pre>
     *  * StringUtils.substring(null, *, *)    = null
     *  * StringUtils.substring("", * ,  *)    = "";
     *  * StringUtils.substring("abc", 0, 2)   = "ab"
     *  * StringUtils.substring("abc", 2, 0)   = ""
     *  * StringUtils.substring("abc", 2, 4)   = "c"
     *  * StringUtils.substring("abc", 4, 6)   = ""
     *  * StringUtils.substring("abc", 2, 2)   = ""
     *  * StringUtils.substring("abc", -2, -1) = "b"
     *  * StringUtils.substring("abc", -4, 2)  = "ab"
     *  * </pre>
     */
    @Test
    public void testSubstring() {
        System.out.println(StringUtils.substring("430000", 0, 2));
        System.out.println(StringUtils.substring("4", 0, 2));
        System.out.println(StringUtils.substring("", 0, 2));
        System.out.println(StringUtils.substring(null, 0, 2));
    }

    /**
     * abbreviate(final String str, final int maxWidth) :对 str 字符串进行省略号缩写，maxWidth 表示长度，必须大于等于4
     * <p>
     * StringUtils.abbreviate(null, *)  = null
     * StringUtils.abbreviate("", 4)  = ""
     * StringUtils.abbreviate("abcdefg", 6) = "abc..."
     * StringUtils.abbreviate("abcdefg", 7) = "abcdefg"
     * StringUtils.abbreviate("abcdefg", 8) = "abcdefg"
     * StringUtils.abbreviate("abcdefg", 4) = "a..."
     * StringUtils.abbreviate("abcdefg", 3) = IllegalArgumentException
     */
    @Test
    public void abbreviate() {
        System.out.println(StringUtils.abbreviate("abcdefg", 7));//= "abcdefg"
        System.out.println(StringUtils.abbreviate("abcdefg", 8));//= "abcdefg"
        System.out.println(StringUtils.abbreviate("abcdefg", 4));//= "a..."
        System.out.println(StringUtils.abbreviate("a", 3));// = a..
        System.out.println(StringUtils.abbreviate("abcdefg", 3));// = IllegalArgumentException
    }

    /**
     * capitalize(final String str) :     将首字母转大写
     * <p>
     * StringUtils.capitalize(null)  = null
     * StringUtils.capitalize("")    = ""
     * StringUtils.capitalize("cat") = "Cat"
     * StringUtils.capitalize("cAt") = "CAt"
     * StringUtils.capitalize("'cat'") = "'cat'"
     */
    @Test
    public void testCapitalize() {

    }

    /**
     * center(final String str, final int size)
     * 将 str 前后用空格填充，使总长度达到 size。size 本身小于 str 长度时，不做填充
     * <p>
     * StringUtils.center(null, *)   = null
     * StringUtils.center("", 4)     = "    "
     * StringUtils.center("ab", -1)  = "ab"
     * StringUtils.center("ab", 4)   = " ab "
     * StringUtils.center("abcd", 2) = "abcd"
     * StringUtils.center("a", 4)    = " a  "
     * <p>
     * center(String str, final int size, String padStr) ：使用指定字符串进行填充
     * <p>
     * StringUtils.center(null, *, *)     = null
     * StringUtils.center("", 4, ' ')     = "    "
     * StringUtils.center("ab", -1, ' ')  = "ab"
     * StringUtils.center("ab", 4, ' ')   = " ab "
     * StringUtils.center("abcd", 2, ' ') = "abcd"
     * StringUtils.center("a", 4, ' ')    = " a  "
     * StringUtils.center("a", 4, 'y')    = "yayy"
     */
    @Test
    public void testCenter() {

    }

    /**
     * contains(CharSequence seq, CharSequence searchSeq) :判断源字符串 seq 是否包含字符串 searchSeq。区分大小写
     * StringUtils.contains(null, *)     = false
     * StringUtils.contains(*, null)     = false
     * StringUtils.contains("", "")      = true
     * StringUtils.contains("abc", "")   = true
     * StringUtils.contains("abc", "a")  = true
     * StringUtils.contains("abc", "z")  = false
     */
    @Test
    public void testContains1() {

    }

    /**
     * contains(CharSequence seq, int searchChar) ：判断源字符串 seq 是否包含字符 searchChar。区分大小写
     * StringUtils.contains(null, *)    = false
     * StringUtils.contains("", *)      = false
     * StringUtils.contains("abc", 'a') = true
     * StringUtils.contains("abc", 'z') = false
     */
    @Test
    public void testContains2() {

    }

    /**
     * containsAny(CharSequence cs, CharSequence searchChars) ：判断源字符串 seq 是否包含字符串 searchChars。不区分大小写
     * <p>
     * StringUtils.containsAny(null, *) = false
     * StringUtils.containsAny("", *)  = false
     * StringUtils.containsAny(*, null) = false
     * StringUtils.containsAny(*, "")  = false
     * StringUtils.containsAny("zzabyycdxx", "za")    = true
     * StringUtils.containsAny("zzabyycdxx", "by")    = true
     * StringUtils.containsAny("zzabyycdxx", "zy")    = true
     * StringUtils.containsAny("zzabyycdxx", "\tx")   = true
     * StringUtils.containsAny("zzabyycdxx", "$.#yF") = true
     * StringUtils.containsAny("aba", "z")            = false
     */
    @Test
    public void testContainsAny1() {

    }

    /**
     * containsAny(CharSequence cs, CharSequence... searchCharSequences)
     * searchCharSequences 数组中主要有任意一个匹配上就返回 true
     * <p>
     * StringUtils.containsAny(null, *) = false
     * StringUtils.containsAny("", *) = false
     * StringUtils.containsAny(*, null) = false
     * StringUtils.containsAny(*, [])              = false
     * StringUtils.containsAny("abcd", "ab", null) = true
     * StringUtils.containsAny("abcd", "ab", "cd") = true
     * StringUtils.containsAny("abc", "d", "abc")  = true
     */
    @Test
    public void testContainsAny2() {

    }

    /**
     * int compare(final String str1, final String str2)：按字典顺序比较两个字符串，不忽略大小写差异，null 值小于非 null值，两个 null 值视为相等.
     * int compare(final String str1, final String str2, final boolean nullIsLess)：
     * 按字典顺序比较两个字符串，不忽略大小写差异，null 值小于非 null值，两个 null 值视为相等, nullIsLess 指示 null 值是否小于非null值
     * int compareIgnoreCase(final String str1, final String str2) ：按字典顺序比较两个字符串，忽略大小写差异，null 值小于非 null值，两个 null 值视为相等.
     * int compareIgnoreCase(final String str1, final String str2, final boolean nullIsLess)
     * * StringUtils.compareIgnoreCase(null, null)   = 0
     * * StringUtils.compareIgnoreCase(null , "a")   < 0
     * * StringUtils.compareIgnoreCase("a", null)    > 0
     * * StringUtils.compareIgnoreCase("abc", "abc") = 0
     * * StringUtils.compareIgnoreCase("abc", "ABC") = 0
     * * StringUtils.compareIgnoreCase("a", "b")     < 0
     * * StringUtils.compareIgnoreCase("b", "a")     > 0
     * * StringUtils.compareIgnoreCase("a", "B")     < 0
     * * StringUtils.compareIgnoreCase("A", "b")     < 0
     * * StringUtils.compareIgnoreCase("ab", "ABC")  < 0
     */
    @Test
    public void testCompareIgnoreCase() {
        System.out.println(StringUtils.compareIgnoreCase("9", "445"));//5
        System.out.println(StringUtils.compareIgnoreCase(null, "a"));//-1
        System.out.println(StringUtils.compareIgnoreCase("abc", "ABC"));//0
        System.out.println("--------------");
        System.out.println(StringUtils.compare("abc", "ABC"));//32
        System.out.println("--------------");
        System.out.println(StringUtils.compare(null, "a", true));//-1
        System.out.println(StringUtils.compare(null, "a", false));//1
    }

    /**
     * countMatches(final CharSequence str, final CharSequence sub) :查找字符串中某个字符或字符串出现的次数
     * <p>
     * StringUtils.countMatches(null, *)       = 0
     * StringUtils.countMatches("", *)         = 0
     * StringUtils.countMatches("abba", null)  = 0
     * StringUtils.countMatches("abba", "")    = 0
     * StringUtils.countMatches("abba", "a")   = 2
     * StringUtils.countMatches("abba", "ab")  = 1
     * StringUtils.countMatches("abba", "xxx") = 0
     */
    @Test
    public void testCountMatches() {

    }

    /**
     * T defaultIfBlank(final T str, final T defaultStr)：如果 str 为 null 或者空字符串，空白字符串，则返回默认值.
     * T defaultIfEmpty(final T str, final T defaultStr)：如果 str 为 null 或者空字符串，则返回默认值.
     * String defaultString(final String str, final String defaultStr)：如果 str 为 null, 则返回默认值.
     */
    @Test
    public void testDefaultIfBlank() {
        String blank1 = StringUtils.defaultIfBlank(null, "*");
        System.out.println(blank1);//*

        System.out.println("--------defaultIfBlank------");
        System.out.println(StringUtils.defaultIfBlank(null, "*"));//*
        System.out.println(StringUtils.defaultIfBlank("", "*"));//*
        System.out.println(StringUtils.defaultIfBlank(" ", "*"));//*
        System.out.println(StringUtils.defaultIfBlank("bat", "*"));//bat
        System.out.println(StringUtils.defaultIfBlank("", null));//null

        System.out.println("--------defaultIfEmpty------");
        System.out.println("" + StringUtils.defaultIfEmpty(null, "*"));//*
        System.out.println(StringUtils.defaultIfEmpty("", "*"));//*
        System.out.println(StringUtils.defaultIfEmpty(" ", "*"));//空白字符串
        System.out.println(StringUtils.defaultIfEmpty("bat", "*"));//bat
        System.out.println(StringUtils.defaultIfEmpty("", null));//null

        System.out.println("--------defaultString------");
        System.out.println(StringUtils.defaultString(null, "*"));//*
        System.out.println(StringUtils.defaultString("", "*"));//空字符串
        System.out.println(StringUtils.defaultString(" ", "*"));//空白字符串
        System.out.println(StringUtils.defaultString("bat", "*"));//bat
        System.out.println(StringUtils.defaultString("", null));//字符串
    }


    /**
     * equals(CharSequence cs1, CharSequence cs2) : 比较两个字符串是否相等，区分大小写
     * <p>
     * StringUtils.equals(null, null)   = true
     * StringUtils.equals(null, "abc")  = false
     * StringUtils.equals("abc", null)  = false
     * StringUtils.equals("abc", "abc") = true
     * StringUtils.equals("abc", "ABC") = false
     */
    @Test
    public void testEquals() {

    }

    /**
     * equalsIgnoreCase(CharSequence cs1, CharSequence cs2) :比较两个字符串是否相等，不区分大小写
     * <p>
     * StringUtils.equalsIgnoreCase(null, null) = true
     * StringUtils.equalsIgnoreCase(null, "abc") = false
     * StringUtils.equalsIgnoreCase("abc", null)  = false
     * StringUtils.equalsIgnoreCase("abc", "abc") = true
     * StringUtils.equalsIgnoreCase("abc", "ABC") = true
     */
    @Test
    public void testEqualsIgnoreCase() {

    }

    /**
     * String join(final Object[] array, final String separator):将数组 array 使用分隔符 separator 连接成字符串
     * String join(final Iterable<?> iterable, final String separator)
     * String join(final Iterator<?> iterator, final String separator)
     * ：对可迭代的元素使用指定字符串进行连接
     * * StringUtils.join(null, *)               = null
     * * StringUtils.join([], *)                 = ""
     * * StringUtils.join([null], *)             = ""
     * * StringUtils.join(["a", "b", "c"], ';')  = "a;b;c"
     * * StringUtils.join(["a", "b", "c"], null) = "abc"
     * * StringUtils.join([null, "", "a"], ';')  = ";;a"
     */
    @Test
    public void testJoin() {
        List<String> list = new ArrayList();
        list.add("01001");
        list.add(null);
        list.add("01003");

        String join1 = StringUtils.join(list, ",");
        String join2 = StringUtils.join(list, "");
        System.out.println(join1);//01001,,01003
        System.out.println(join2);//0100101003
    }

    /**
     * boolean isNumeric(final CharSequence cs)：检查 cs 是否只包含 Unicode 数字，小数点不是 Unicode 数字，返回 false
     * 该方法不允许前导符号，无论是正号还是负号
     * * StringUtils.isNumeric(null)   = false
     * * StringUtils.isNumeric("")     = false
     * * StringUtils.isNumeric("  ")   = false
     * * StringUtils.isNumeric("123")  = true
     * * StringUtils.isNumeric("\u0967\u0968\u0969")  = true
     * * StringUtils.isNumeric("09") = true
     * * StringUtils.isNumeric("12 3") = false
     * * StringUtils.isNumeric("ab2c") = false
     * * StringUtils.isNumeric("12-3") = false
     * * StringUtils.isNumeric("12.3") = false
     * * StringUtils.isNumeric("-123") = false
     * * StringUtils.isNumeric("+123") = false
     */
    @Test
    public void testIsNumeric() {
        System.out.println(StringUtils.isNumeric("00389238920823"));//true
    }

    /**
     * isNumericSpace(CharSequence cs) :判断字符串是否是数字，忽略空格
     * <p>
     * StringUtils.isNumericSpace(null)   = false
     * StringUtils.isNumericSpace("")     = true
     * StringUtils.isNumericSpace("  ")   = true
     * StringUtils.isNumericSpace("123")  = true
     * StringUtils.isNumericSpace("12 3") = true
     * StringUtils.isNumeric("\u0967\u0968\u0969")  = true
     * StringUtils.isNumeric("\u0967\u0968 \u0969")  = true
     * StringUtils.isNumericSpace("ab2c") = false
     * StringUtils.isNumericSpace("12-3") = false
     * StringUtils.isNumericSpace("12.3") = false
     */
    @Test
    public void testIsNumericSpace() {

    }

    /**
     * isAlpha(CharSequence cs) ：判断字符串是否是希腊字母，不忽略空格
     * StringUtils.isAlpha(null)   = false
     * StringUtils.isAlpha("")     = false
     * StringUtils.isAlpha("  ")   = false
     * StringUtils.isAlpha("abc")  = true
     * StringUtils.isAlpha("ab2c") = false
     * StringUtils.isAlpha("ab-c") = false
     * <p>
     * isAlphaSpace(CharSequence cs) ： 判断字符串是否是希腊字母，忽略空格
     * StringUtils.isAlphaSpace(null)   = false
     * StringUtils.isAlphaSpace("")     = true
     * StringUtils.isAlphaSpace("  ")   = true
     * StringUtils.isAlphaSpace("abc")  = true
     * StringUtils.isAlphaSpace("ab c") = true
     * StringUtils.isAlphaSpace("ab2c") = false
     * StringUtils.isAlphaSpace("ab-c") = false
     */
    @Test
    public void testIsAlpha() {

    }

    /**
     * isAlphanumeric(CharSequence cs) ：判断字符串是否是希腊字母与数字组成，不忽略空格
     * isAlphanumericSpace(CharSequence cs) ：判断字符串是否是希腊字母与数字组成，忽略空格
     */
    @Test
    public void testIsAlphanumeric() {
        System.out.println(StringUtils.isAlphanumeric("ab c"));//false
        System.out.println(StringUtils.isAlphanumeric("abC12"));//true
        System.out.println(StringUtils.isAlphanumeric("ABC"));//true
        System.out.println(StringUtils.isAlphanumeric("ABC4378"));//true
        System.out.println(StringUtils.isAlphanumeric("但是"));//true
        System.out.println(StringUtils.isAlphanumericSpace("ab c"));//true
        System.out.println(StringUtils.isAlphanumericSpace("ab cAUI988"));//true
        System.out.println(StringUtils.isAlphanumericSpace("ab cAUI988.89"));//false
        System.out.println(StringUtils.isAlphanumericSpace("发就邓"));//true
    }

    /**
     * isBlank(CharSequence cs) :判断目标字符串为空或者为null，空格也当作为空
     * StringUtils.isBlank(" ")   = true
     * StringUtils.isBlank("bob")    = false
     * StringUtils.isBlank("  bob  ") = false
     * StringUtils.isBlank(null)  = true
     * StringUtils.isBlank("")    = tru
     */
    @Test
    public void testIsBlank() {
        System.out.println(StringUtils.isBlank(" ")); // true
        System.out.println(StringUtils.isBlank(null)); // true
        System.out.println(StringUtils.isBlank("")); // true
        System.out.println(StringUtils.isBlank("  bob  ")); // false
    }

    /**
     * isAllLowerCase(CharSequence cs) ： 判断字符串是否全是小写字母
     * StringUtils.isAllLowerCase(null)   = false
     * StringUtils.isAllLowerCase("")     = false
     * StringUtils.isAllLowerCase("  ")   = false
     * StringUtils.isAllLowerCase("abc")  = true
     * StringUtils.isAllLowerCase("abC")  = false
     * StringUtils.isAllLowerCase("ab c") = false
     * StringUtils.isAllLowerCase("ab1c") = false
     * StringUtils.isAllLowerCase("ab/c") = false
     */
    @Test
    public void testIsAllLowerCase() {

    }

    /**
     * isAllUpperCase(CharSequence cs) : 判断字符串是否全大写字母
     * StringUtils.isAllUpperCase(null)   = false
     * StringUtils.isAllUpperCase("")     = false
     * StringUtils.isAllUpperCase("  ")   = false
     * StringUtils.isAllUpperCase("ABC")  = true
     * StringUtils.isAllUpperCase("aBC")  = false
     * StringUtils.isAllUpperCase("A C")  = false
     * StringUtils.isAllUpperCase("A1C")  = false
     * StringUtils.isAllUpperCase("A/C")  = false
     */
    @Test
    public void testIsAllUpperCase() {

    }

    /**
     * isNotBlank(CharSequence cs) : 判断目标字符串不为空且不为null，空格也当作为空
     * StringUtils.isNotBlank(null) = false
     * StringUtils.isNotBlank("")    = false
     * StringUtils.isNotBlank(" ")   = false
     * StringUtils.isNotBlank("bob")     = true
     * StringUtils.isNotBlank("  bob  ")  = true
     */
    @Test
    public void testIsNotBlank() {

    }

    /**
     * isEmpty(CharSequence cs) ：判断目标字符串为空或者为null，空格不当作为空
     * StringUtils.isEmpty(null) = true
     * StringUtils.isEmpty("")    = true
     * StringUtils.isEmpty(" ")   = false
     * StringUtils.isEmpty("bob")     = false
     * StringUtils.isEmpty("  bob  ")  = false
     */
    @Test
    public void testIsEmpty() {

    }

    /**
     * isNotEmpty(CharSequence cs) ：判断目标字符串不为空且不为null，空格不当作为空
     * <p>
     * StringUtils.isNotEmpty(null)  = false
     * StringUtils.isNotEmpty("")    = false
     * StringUtils.isNotEmpty(" ")  = true
     * StringUtils.isNotEmpty("bob")   = true
     * StringUtils.isNotEmpty("  bob  ") = true
     */
    @Test
    public void testIsNotEmpty() {

    }

    /**
     * trim(String str) : 去掉字符串前后空格。为空时返回空，为 null 时返回 null(不是字符串null，防止空指针异常)
     * <p>
     * StringUtils.trim(null)   = null
     * StringUtils.trim("")  = ""
     * StringUtils.trim("     ")       = ""
     * StringUtils.trim("abc")         = "abc"
     * StringUtils.trim("    abc    ")
     */
    @Test
    public void testTrim() {
        // ab c
        System.out.println(StringUtils.trim(" ab c \t\n\r  "));
    }

    /**
     * trimToNull(String str) : 去掉字符串前后空格。当为空时，返回 null(不是字符串null，防止空指针异常)
     * <p>
     * StringUtils.trimToNull(null)          = null
     * StringUtils.trimToNull("")            = null
     * StringUtils.trimToNull("     ")       = null
     * StringUtils.trimToNull("abc")         = "abc"
     * StringUtils.trimToNull("    abc    ") = "abc"
     */
    @Test
    public void testTrimToNull() {

    }

    /**
     * trimToEmpty(String str) :去掉字符串前后空格。当为 null 时，返回空
     * <p>
     * StringUtils.trimToEmpty(null)          = ""
     * StringUtils.trimToEmpty("")            = ""
     * StringUtils.trimToEmpty("     ")       = ""
     * StringUtils.trimToEmpty("abc")         = "abc"
     * StringUtils.trimToEmpty("    abc    ") = "abc"
     */
    @Test
    public void testTrimToEmpty() {

    }

    /**
     * indexOf(CharSequence seq, CharSequence searchSeq)
     * 查找 seq 中 searchSeq 第一次出现的索引位置，找不到返回 -1，区分大小写。
     * <p>
     * StringUtils.indexOf(null, *)          = -1
     * StringUtils.indexOf(*, null)          = -1
     * StringUtils.indexOf("", "")           = 0
     * StringUtils.indexOf("", *)     = -1 (except when = "")
     * StringUtils.indexOf("aabaabaa", "a")  = 0
     * StringUtils.indexOf("aabaabaa", "b")  = 2
     * StringUtils.indexOf("aabaabaa", "ab") = 1
     * StringUtils.indexOf("aabaabaa", "")   =0
     * <p>
     * indexOf(final CharSequence seq, final CharSequence searchSeq, final int startPos)
     * 从指定位置开始查找
     * <p>
     * StringUtils.indexOf(null, *, *) = -1
     * StringUtils.indexOf(*, null, *) = -1
     * StringUtils.indexOf("", "", 0) = 0
     * StringUtils.indexOf("", *, 0) = -1
     * StringUtils.indexOf("aabaabaa", "a", 0)  = 0
     * StringUtils.indexOf("aabaabaa", "b", 0)  = 2
     * StringUtils.indexOf("aabaabaa", "ab", 0) = 1
     * StringUtils.indexOf("aabaabaa", "b", 3)  = 5
     * StringUtils.indexOf("aabaabaa", "b", 9)  = -1
     * StringUtils.indexOf("aabaabaa", "b", -1) = 2
     * StringUtils.indexOf("aabaabaa", "", 2)   = 2
     * StringUtils.indexOf("abc", "", 9)        = 3
     */
    @Test
    public void testIndexOf1() {

    }

    /**
     * 查找数组中任意元素在源字符串中出现的索引位置，满足多个时，取最小值
     * indexOfAny(final CharSequence str, final CharSequence... searchStrs)
     * <p>
     * StringUtils.indexOfAny(null, *) = -1
     * StringUtils.indexOfAny(*, null) = -1
     * StringUtils.indexOfAny(*, []) = -1
     * StringUtils.indexOfAny("zzabyycdxx", ["ab", "cd"]) = 2
     * StringUtils.indexOfAny("zzabyycdxx", ["cd", "ab"])   = 2
     * StringUtils.indexOfAny("zzabyycdxx", ["mn", "op"]) = -1
     * StringUtils.indexOfAny("zzabyycdxx", ["zab", "aby"])=1
     * StringUtils.indexOfAny("zzabyycdxx", [""]) = 0
     * StringUtils.indexOfAny("", [""])   = 0
     * StringUtils.indexOfAny("", ["a"]) = -1
     */
    @Test
    public void testIndexOfAny() {

    }

    /**
     * lowerCase(final String str) :字符字母转小写
     * <p>
     * StringUtils.lowerCase(null)  = null
     * StringUtils.lowerCase("")    = ""
     * StringUtils.lowerCase("aBc") = "abc"
     * <p>
     * upperCase(final String str) ：字符串字母转大写
     * <p>
     * StringUtils.upperCase(null)  = null
     * StringUtils.upperCase("")    = ""
     * StringUtils.upperCase("aBc") = "ABC"
     */
    @Test
    public void testLowerCase() {

    }

    /**
     * String strip(final String str)
     * 1、从字符串的开头和结尾删除空格(空白字符串)，类似 trim(xxx)
     * String[] stripAll(final String... strs)
     * 1、批量删除，每次返回一个新数组，长度为零除外。
     * * StringUtils.stripAll(null)             = null
     * * StringUtils.stripAll([])               = []
     * * StringUtils.stripAll(["abc", "  abc"]) = ["abc", "abc"]
     * * StringUtils.stripAll(["abc  ", null])  = ["abc", null]
     */
    @Test
    public void testStrip1() {
        // null
        System.out.println(StringUtils.strip(null));
        // ""
        System.out.println(StringUtils.strip(""));
        // ""
        System.out.println(StringUtils.strip("   "));
        // "abc"
        System.out.println(StringUtils.strip("abc"));
        // "abc"
        System.out.println(StringUtils.strip("  abc"));
        // "abc"
        System.out.println(StringUtils.strip("abc  "));
        // "abc"
        System.out.println(StringUtils.strip(" abc "));
        // "ab c"
        System.out.println(StringUtils.strip(" ab c "));
        // ab c
        System.out.println(StringUtils.strip(" ab c \t\n\r  "));
    }

    /**
     * String strip(String str, final String stripChars)
     * 1、从字符串的开头和结尾删除指定的一组字符(stripChars)
     * 2、会从 str 的开头和结尾删除出现的 stripChars 字符串中的全部字符
     * * StringUtils.strip(null, *)          = null
     * * StringUtils.strip("", *)            = ""
     * * StringUtils.strip("  abcyx", "xyz") = "  abc"
     */
    @Test
    public void testStrip2() {
        // null
        System.out.println(StringUtils.strip(null, null));
        // ""
        System.out.println(StringUtils.strip("", null));
        // ""
        System.out.println(StringUtils.strip("   ", null));
        // "abc"
        System.out.println(StringUtils.strip("abc", null));
        // "abc"
        System.out.println(StringUtils.strip("  abc", null));
        // "abc"
        System.out.println(StringUtils.strip("abc  ", null));
        // "abc"
        System.out.println(StringUtils.strip(" abc ", null));
        // "ab c"
        System.out.println(StringUtils.strip(" ab c ", null));
        // "ab c"
        System.out.println(StringUtils.strip(" ab c \t\n\r  ", null));
        // "  abc"
        System.out.println(StringUtils.strip("  abcyx", "xyz"));
        // 1,2,3,4,5
        System.out.println(StringUtils.strip("[1,2,3,4,5]", "[]"));
    }

    /**
     * String stripEnd(final String str, final String stripChars)
     * 1、从字符串的结尾删除指定的一组字符(stripChars)。
     * * StringUtils.stripEnd(null, *)          = null
     * * StringUtils.stripEnd("", *)            = ""
     * * StringUtils.stripEnd("abc", "")        = "abc"
     * * StringUtils.stripEnd("abc", null)      = "abc"
     * * StringUtils.stripEnd("  abc", null)    = "  abc"
     * * StringUtils.stripEnd("abc  ", null)    = "abc"
     * * StringUtils.stripEnd(" abc ", null)    = " abc"
     * * StringUtils.stripEnd("  abcyx", "xyz") = "  abc"
     * * StringUtils.stripEnd("120.00", ".0")   = "12"
     * String stripStart(final String str, final String stripChars)
     * 1、从字符串的开头删除指定的一组字符(stripChars)
     */
    @Test
    public void testStrip3() {
        // "12"
        System.out.println(StringUtils.stripEnd("120.00", ".0"));
        // "abc"
        System.out.println(StringUtils.stripStart("yxabc  ", "xyz"));
    }

    /**
     * String deleteWhitespace(final String str) ：从字符串中删除所有空格
     * <pre>
     * StringUtils.deleteWhitespace(null)         = null
     * StringUtils.deleteWhitespace("")           = ""
     * StringUtils.deleteWhitespace("abc")        = "abc"
     * StringUtils.deleteWhitespace("   ab  c  ") = "abc"
     * </pre>
     */
    @Test
    public void testDeleteWhitespace() {
        System.out.println(StringUtils.deleteWhitespace(null));// null
        System.out.println(StringUtils.deleteWhitespace(" ")); //空字符串
        System.out.println(StringUtils.deleteWhitespace(" love you ! "));// loveyou!
        System.out.println(StringUtils.deleteWhitespace(" love \n\ryou \t! ")); // loveyou!
        System.out.println(StringUtils.deleteWhitespace(" love\n" + "you\n" + "！"));// loveyou！
    }

}
