package com.wmx.apachestudy.lang;

import org.apache.commons.text.StringEscapeUtils;
import org.junit.Test;

/**
 * StringEscapeUtils最初是Apache Commons Lang库的一部分（2.0版本引入），用于处理字符串的转义与反转义。
 * 由于模块化调整，自Commons Lang 3.6起，该类被标记为过时并迁移至Commons Text包。
 * 该类所有方法均为静态且线程安全，无需实例化即可直接调用。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2025/3/6 上午9:03
 */
public class StringEscapeUtilsTest {

    /**
     * HTML转义(防止XSS攻击) & 反转义
     */
    @Test
    public void testHtmlEscape() throws Exception {
        String escapedHtml = StringEscapeUtils.escapeHtml4("<script>alert('xss')</script>");
        // &lt;script&gt;alert('xss')&lt;/script&gt;
        System.out.println(escapedHtml);

        String original = StringEscapeUtils.unescapeHtml4(escapedHtml);
        // <script>alert('xss')</script>
        System.out.println(original);
    }

    /**
     * Java字符串转义
     * 处理控制字符（如换行符\n转为\\n）和引号
     */
    @Test
    public void testJavaEscape() {
        String text = "He said, \"Stop!\"\n";
        String escaped = StringEscapeUtils.escapeJava(text);
        System.out.println(text); // He said, "Stop!"
        System.out.println(escaped); // He said, \"Stop!\"\n
        // 反转义则使用unescapeJava()
        System.out.println(StringEscapeUtils.unescapeJava(escaped));// He said, "Stop!"
    }

    /**
     * XML转义
     */
    @Test
    public void testXmlEscape() {
        String text = "<value>5 > 3</value>";
        String xml1 = StringEscapeUtils.escapeXml10(text);
        String xml2 = StringEscapeUtils.escapeXml11(text);

        System.out.println(xml1); // &lt;value&gt;5 &gt; 3&lt;/value&gt;
        System.out.println(xml2); // &lt;value&gt;5 &gt; 3&lt;/value&gt;

        System.out.println(StringEscapeUtils.unescapeXml(xml1)); // <value>5 > 3</value>
        System.out.println(StringEscapeUtils.unescapeXml(xml2)); // <value>5 > 3</value>
    }

    /**
     * CSV转义，处理逗号和引号
     */
    @Test
    public void testCvsEscape() {
        String text = "Data, \"with\" quotes";
        String csv = StringEscapeUtils.escapeCsv(text);

        System.out.println(text); // Data, "with" quotes
        System.out.println(csv); // "Data, ""with"" quotes"
        System.out.println(StringEscapeUtils.unescapeCsv(csv)); // Data, "with" quotes
    }

    /**
     * JavaScript转义
     */
    @Test
    public void testScriptEscape() {
        String js = StringEscapeUtils.escapeEcmaScript("let a= <div>I'm a string;</div>");
        System.out.println(js);// let a= <div>I\'m a string;<\/div>
        System.out.println(StringEscapeUtils.unescapeEcmaScript(js)); // let a= <div>I'm a string;</div>
    }

    /**
     * Json转义
     */
    @Test
    public void testJsonEscape() {
        String text = "{\"name\": \"He didn't say, \\\"Stop!\\\"\"}";
        String json = StringEscapeUtils.escapeJson(text);

        System.out.println(text); // {"name": "He didn't say, \"Stop!\""}
        System.out.println(json); // {\"name\": \"He didn't say, \\\"Stop!\\\"\"}
        System.out.println(StringEscapeUtils.unescapeJson(json)); // {"name": "He didn't say, \"Stop!\""}
    }
}
