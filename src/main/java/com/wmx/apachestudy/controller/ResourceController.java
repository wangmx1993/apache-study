package com.wmx.apachestudy.controller;

import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/3/8 0008.
 * 资源控制器
 */
@RestController
public class ResourceController {

    /**
     * 单文件上传: http://localhost:8080/file/upload?info=文件上传
     *
     * @param file ：上传的文件封装好的对象
     * @param info ：关于文件的描述
     */
    @PostMapping("file/upload")
    public Map<String, Object> singleFileUpload(@RequestParam MultipartFile file, String info) {
        /**获取上传文件名以及大小
         *fileNameAndFormat:包含文件名与格式，如:123.mp4
         *fileName:文件名，如：123
         *fileFormat：文件格式，如：.mp4
         *fileSizeLong：文件大小，long型，单位字节，如：1024
         *fileSizeStr:文件大小，字符型，如：10M
         * fileType：文件类型
         * */
        String fileNameAndFormat = file.getOriginalFilename();
        String fileName = fileNameAndFormat.substring(0, fileNameAndFormat.lastIndexOf("."));
        String fileFormat = fileNameAndFormat.substring(fileNameAndFormat.lastIndexOf("."));
        Long fileSizeLong = file.getSize();
        String fileSizeStr = FileUtils.byteCountToDisplaySize(fileSizeLong);

        System.out.println("fileNameAndFormat=" + fileNameAndFormat);
        System.out.println("fileName=" + fileName);
        System.out.println("fileFormat=" + fileFormat);
        System.out.println("fileSizeLong=" + fileSizeLong);
        System.out.println("fileSizeStr=" + fileSizeStr);
        System.out.println("info=" + info);

        Map<String, Object> dataMpa = new HashMap<>();
        dataMpa.put("code", 200);
        dataMpa.put("msg", "success");
        dataMpa.put("data", null);
        return dataMpa;
    }

    /**
     * 多文件上传 ：http://localhost:8080/file/uploads?info=多文件上传
     *
     * @param files ：SpringMVC文件上传封装对象，多文件上传时，使用MultipartFile列表(List)或者数组(Array)都可以 名称要与前台的 <input type="file" name="uploadFileList" multiple/>的name属性值一致
     * @param info  ：用户输入的文件描述
     */
    @PostMapping("file/uploads")
    public Map<String, Object> multipartFileUpload(@RequestParam List<MultipartFile> files, String info) {
        if (files != null && files.size() > 0) {
            for (MultipartFile uploadFile : files) {
                /**获取上传文件名以及大小
                 *fileNameAndFormat:包含文件名与格式，如:123.mp4
                 *fileName:文件名，如：123
                 *fileFormat：文件格式，如：.mp4
                 *fileSizeLong：文件大小，long型，单位字节，如：1024
                 *fileSizeStr:文件大小，字符型，如：10M
                 * fileType：文件类型
                 * */
                String fileNameAndFormat = uploadFile.getOriginalFilename();
                Long fileSizeLong = uploadFile.getSize();
                String fileSizeStr = FileUtils.byteCountToDisplaySize(fileSizeLong);

                System.out.println("fileNameAndFormat=" + fileNameAndFormat);
                System.out.println("fileSizeStr=" + fileSizeStr);
                System.out.println("info=" + info);
            }
        }
        Map<String, Object> dataMpa = new HashMap<>();
        dataMpa.put("code", 200);
        dataMpa.put("msg", "success");
        dataMpa.put("data", null);
        return dataMpa;
    }

    /**
     * put 请求:  http://localhost:8080/example/httpPut?info=put请求
     *
     * @param dataMap
     * @return
     */
    @PutMapping("example/httpPut")
    public Map<String, Object> httpPut(@RequestBody Map<String, Object> dataMap, String info) {
        System.out.println("dataMap=" + dataMap);
        System.out.println("info=" + info);

        Map<String, Object> dataMpa = new HashMap<>();
        dataMpa.put("code", 200);
        dataMpa.put("msg", "success");
        dataMpa.put("data", dataMap);
        return dataMpa;
    }

    /**
     * delete 请求:  http://localhost:8080/example/httpDelete?info=delete请求
     *
     * @param info
     * @return
     */
    @DeleteMapping("example/httpDelete")
    public Map<String, Object> httpDelete(String info) {
        System.out.println("info =" + info);

        Map<String, Object> dataMpa = new HashMap<>();
        dataMpa.put("code", 200);
        dataMpa.put("msg", "success");
        dataMpa.put("data", null);
        System.out.println(1/0);
        return dataMpa;
    }
}