package com.wmx.guava;

import com.google.common.util.concurrent.RateLimiter;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * RateLimiter 限速器/限流器
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/11/4 15:42
 */
@SuppressWarnings("Duplicates")
public class RateLimiterTest {

    private static final Logger log = LoggerFactory.getLogger(RateLimiterTest.class);

    @Test
    public void testRateLimiter1() {
        RateLimiter rateLimiter = RateLimiter.create(1);
        for (int i = 0, max = 10; i < max; i++) {
            new Thread(() -> {
                // boolean tryAcquire = rateLimiter.tryAcquire();
                boolean tryAcquire = rateLimiter.tryAcquire(Duration.ofSeconds(2));
                if (tryAcquire) {
                    log.info("获得许可证，执行任务，当前速率：" + rateLimiter.getRate() + "/s");
                } else {
                    log.error("未获得许可证，无法执行任务，当前速率：" + rateLimiter.getRate() + "/s");
                }
            }).start();
            try {
                TimeUnit.MILLISECONDS.sleep(new Random().nextInt(800));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // 2023-11-04 16:56:14.834 ==> [Thread-1] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:15.673 ==> [Thread-4] ==> ERROR com.wmx.guava.RateLimiterTest - 未获得许可证，无法执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:15.787 ==> [Thread-2] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:16.572 ==> [Thread-6] ==> ERROR com.wmx.guava.RateLimiterTest - 未获得许可证，无法执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:16.783 ==> [Thread-3] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:17.389 ==> [Thread-8] ==> ERROR com.wmx.guava.RateLimiterTest - 未获得许可证，无法执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:17.445 ==> [Thread-9] ==> ERROR com.wmx.guava.RateLimiterTest - 未获得许可证，无法执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:17.784 ==> [Thread-5] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:18.784 ==> [Thread-7] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s
        // 2023-11-04 16:56:19.783 ==> [Thread-10] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s

        Scanner scanner = new Scanner(System.in);
        scanner.next();
    }

    @Test
    public void testRateLimiter2() {
        RateLimiter rateLimiter = RateLimiter.create(1);
        for (int i = 0, max = 10; i < max; i++) {
            new Thread(() -> {
                double acquire = rateLimiter.acquire();
                log.info("获得许可证，执行任务，当前速率：{}/s 等待时间 {}", rateLimiter.getRate(), acquire);
            }).start();
            try {
                TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // 2023-11-04 16:57:34.480 ==> [Thread-1] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 0.0
        // 2023-11-04 16:57:35.417 ==> [Thread-2] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 0.193155
        // 2023-11-04 16:57:36.417 ==> [Thread-3] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 0.609643
        // 2023-11-04 16:57:37.416 ==> [Thread-4] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 1.251649
        // 2023-11-04 16:57:38.415 ==> [Thread-5] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 1.908182
        // 2023-11-04 16:57:39.416 ==> [Thread-6] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 2.365081
        // 2023-11-04 16:57:40.416 ==> [Thread-7] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 3.319103
        // 2023-11-04 16:57:41.416 ==> [Thread-8] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 3.349201
        // 2023-11-04 16:57:42.416 ==> [Thread-9] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 3.391127
        // 2023-11-04 16:57:43.417 ==> [Thread-10] ==> INFO  com.wmx.guava.RateLimiterTest - 获得许可证，执行任务，当前速率：1.0/s 等待时间 3.91561

        Scanner scanner = new Scanner(System.in);
        scanner.next();
    }

}
