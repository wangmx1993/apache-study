package com.wmx.jdk8;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import com.wmx.apachestudy.pojo.Person;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 1、jdk 1.8 的 java.util.Stream 表示能应用在一组元素上一次执行的操作序列。
 * 2、Stream 操作分为中间操作或者最终操作两种，最终操作返回一特定类型的计算结果，而中间操作继续返回 Stream，方便链式操作。
 * 3、Stream 的创建需要指定一个数据源，比如 java.util.Collection 的子类，List 或者 Set， Map不支持。
 * 4、Stream 的操作可以串行 stream() 执行或者并行 parallelStream() 执行。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/6/10 20:00
 */
@SuppressWarnings("all")
public class StreamTest {

    /**
     * forEach(Consumer<? super T> action) 对该流中的每个元素执行最终操作
     * 1、实现 {@link Consumer} 接口中 void accept(T t) 方法，t 表示流中当前遍历到的元素
     * 2、Stream<E> stream()：将集合转为流
     */
    @Test
    public void forEach1() {
        List<String> list = Arrays.asList("长沙", "深圳", "武汉", "伊犁", "洛阳", "开封");
        // 方式一：增强 for 循环
        for (String item : list) {
            System.out.print(item + "\t");
        }
        System.out.println();
        // 方式二：JDK 1.8 java.util.stream.Stream.forEach(Consumer<? super T> action)
        //长沙	深圳	武汉	伊犁	洛阳	开封
        list.stream().forEach(item -> System.out.print(item + "\t"));
    }

    /**
     * 遍历元素的时候，修改元素的值。比如遍历修改 List 中的对象
     */
    @Test
    public void forEach2() {
        List<Map<String, Object>> dataList = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> map2 = new HashMap<>();
        map1.put("c20", "新增");
        map2.put("c20", "既往");
        dataList.add(map1);
        dataList.add(map2);

        dataList.stream().forEach(item -> item.put("c20", "删除"));
        // [{c20=删除}, {c20=删除}]
        System.out.println(dataList);
    }

    /**
     * Stream<T> filter(Predicate<? super T> predicate) 元素过滤，返回符合条件的元素，这是一个中间操作，返回结果流。
     * 元素不能为 null，否则空指针异常。
     * 1、需要实现 {@link Predicate} 接口中的 boolean test(T t) 方法,t 为流中的元素，返回 false 的元素将会丢弃.
     */
    @Test
    public void filter1() {
        List<Integer> integerList = Arrays.asList(8, 12, 28, 19, 22, 39, 33, 44, 54, 33, 23);
        integerList.stream().filter(integer -> integer >= 18 && integer < 45).forEach(integer -> {
            //28	19	22	39	33	44	33	23
            System.out.print(integer + "\t");
        });
    }

    /**
     * 元素过滤
     * T Optional.get()：如果存在值，则返回，否则抛出异常 NoSuchElementException
     */
    @Test
    public void filter2() {
        Object[] res = Stream.of(1, 2, 3, 4, 5, 6, 7, 8).filter(i -> i % 2 == 0).filter(i -> i > 3).toArray();
        //[4, 6, 8]
        System.out.println(Arrays.toString(res));

        //[2, 4, 6, 8]
        List<Integer> collect = Stream.of(1, 2, 3, 4, 5, 6, 7, 8).filter(i -> i % 2 == 0).collect(Collectors.toList());
        System.out.println(collect);

        Optional<Integer> first = Stream.of(1, 2, 3, 4, 5, 6, 7, 8).filter(i -> i % 2 == 0).findFirst();
        //2
        System.out.println(first.isPresent() ? first.get() : null);

        Optional<Integer> integer = Stream.of(1, 2, 3, 4, 5, 6, 7, 8).filter(i -> i % 2 == 0).parallel().findAny();
        // 6
        System.out.println(integer.get());
    }

    /**
     * 排除掉 c21=删除 的单位
     */
    @Test
    public void filter3() {
        List<Map<String, Object>> dataList = this.getDataList();
        // [{c21=新增, address=深圳市, agency_code=201025},
        // {c21=既往, address=长沙市, agency_code=002015},
        // {c21=删除, address=武汉市, agency_code=304100},
        // {c21=既往, address=深圳市, agency_code=324100}]
        System.out.println(dataList);

        List<Map<String, Object>> collect = dataList.stream().filter(item -> !"删除".equals(item.get("c21"))).collect(Collectors.toList());
        // [{c21=新增, address=深圳市, agency_code=201025},
        // {c21=既往, address=长沙市, agency_code=002015},
        // {c21=既往, address=深圳市, agency_code=324100}]
        System.out.println(collect);
    }

    @Test
    public void filter4() {
        Date date = new Date();
        Person person1 = new Person(1, "张三", date, 7888.89f);
        Person person2 = new Person(2, "李四", date, 5688.71f);
        Person person3 = new Person(3, "王五", date, 8588.53f);
        Person person4 = new Person(4, "马六", date, null);
        Person person5 = new Person(4, "马六", date, 1288.35f);
        List<Person> persons = ListUtil.toList(person1, person2, person3, person4, person5);

        // 过滤工资大于6000的人员
        List<Person> filterPersons = persons.stream().filter(p -> p.getSalary() == null ? 1 == 2 : p.getSalary() > 6000.00F).collect(Collectors.toList());

        // Person{id=1, name='张三', birthday=Sat Nov 25 16:52:41 CST 2023, salry=7888.89}
        // Person{id=3, name='王五', birthday=Sat Nov 25 16:52:41 CST 2023, salry=8588.53}
        filterPersons.stream().forEach(item -> System.out.println(item));
    }

    /**
     * 过滤 Map
     */
    @Test
    public void filterMap1() {
        Map<String, Person> personMap = new HashMap<>();
        Person person1 = new Person(1, "张三", DateUtil.parse("1993/08/25"), 7888.89f);
        Person person2 = new Person(2, "李四", DateUtil.parse("1994/08/21"), 5688.71f);
        Person person3 = new Person(3, "王五", DateUtil.parse("1995/08/15"), 8588.53f);
        Person person4 = new Person(4, "马六", DateUtil.parse("1991/06/13"), null);
        Person person5 = new Person(4, "马六", DateUtil.parse("1992/04/05"), 1288.35f);

        personMap.put("001", person1);
        personMap.put("002", person2);
        personMap.put("003", person3);
        personMap.put("004", person4);
        personMap.put("005", person5);

        // 过滤工资大于6000的人员
        Map<String, Person> collect = personMap.entrySet().stream().filter(entry -> entry.getValue().getSalary() != null && entry.getValue().getSalary() > 6000)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        // {
        // 001=Person{id=1, name='张三', birthday=1993-08-25 00:00:00, salary=7888.89},
        // 003=Person{id=3, name='王五', birthday=1995-08-15 00:00:00, salary=8588.53}
        // }
        System.out.println(collect);
    }

    /**
     * Object[] toArray()：返回包含此流元素的数组。
     */
    @Test
    public void toArrayTest1() {
        Object[] res = Stream.of(1, 2, 3, 4, 5, 6, 7, 8).filter(i -> i % 2 == 0).filter(i -> i > 3).toArray();
        // [4, 6, 8]
        System.out.println(Arrays.toString(res));

        Integer[] integers = Stream.of(1, 2, 3, 4, 5, 6, 7, 8).filter(i -> i % 2 == 0).filter(i -> i > 3).toArray(Integer[]::new);
        // [4, 6, 8]
        System.out.println(Arrays.toString(res));
    }

    @Test
    public void toArrayTest2() {
        Date date = new Date();
        Person person1 = new Person(1, "张三", date, 7888.89f);
        Person person2 = new Person(2, "李四", date, 5688.71f);
        Person person3 = new Person(3, "王五", date, 8588.53f);
        Person person4 = new Person(4, "马六", date, null);
        Person person5 = new Person(4, "马六", date, 4512F);
        List<Person> persons = ListUtil.toList(person1, person2, person3, person4, person5);

        Person[] adultsArray = persons.stream().filter(p -> (p.getSalary() == null ? 0 : p.getSalary()) > 5000).toArray(Person[]::new);
        // Person{id=1, name='张三', birthday=Sun Dec 03 16:47:42 CST 2023, salry=7888.89}
        // Person{id=2, name='李四', birthday=Sun Dec 03 16:47:42 CST 2023, salry=5688.71}
        // Person{id=3, name='王五', birthday=Sun Dec 03 16:47:42 CST 2023, salry=8588.53}
        Arrays.asList(adultsArray).stream().forEach(item -> System.out.println(item));
    }

    /**
     * Optional<T> findFirst()：返回描述此流的第一个元素的{@link Optional}，如果流为空，则返回空的{@code Optional}
     * Optional<T> findAny(): 返回描述此流的任意一个元素的{@link Optional}，如果流为空，则返回空的{@code Optional}
     */
    @Test
    public void findAnyTest1() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        // 获取列表中的第一个数字
        Optional<Integer> firstNumber = numbers.stream().findFirst();
        // Optional[1]
        System.out.println(firstNumber);

        // 获取列表中的任意一个数字
        Optional<Integer> anyNumber = numbers.stream().findAny();
        // 输出结果为 Optional[1] 或者 Optional[2] 或者其他任意一个数字
        System.out.println(anyNumber);
    }

    /**
     * 映射，可以将一个流的元素按照一定的映射规则映射到另一个流中。分为map和flatMap。
     * map：接收一个函数作为参数，该函数会被应用到每个元素上，并将其映射成一个新的元素。
     * flatMap：接收一个函数作为参数，将流中的每个值都换成另一个流，然后把所有流连接成一个流。
     * <p>
     * Stream<R> map(Function<? super T, ? extends R> mapper)
     * 1、元素映射，实质就是用于处理元素，然后返回处理结果，这是一个中间操作，返回一个结果流
     * <p>
     * 如果你的流可能包含大量不符合你的条件的元素，请在 map() 之前使用 filter() 以避免不必要的处理。这可以提高代码的性能。
     */
    @Test
    public void map1() {
        List<String> list = Arrays.asList("how", "are", "you", ",", "I", "am", "fine", "!");
        //对每个单词的首字母转大写
        Stream<String> stringStream = list.stream().map(item -> {
            if (item.length() > 1) {
                return item.substring(0, 1).toUpperCase() + "" + item.substring(1);
            } else {
                return item;
            }
        });
        //[how, are, you, ,, I, am, fine, !]
        System.out.println(list);
        //How Are You , I Am Fine !
        stringStream.forEach(item -> System.out.print(item + " "));
    }

    @Test
    public void map2() {
        List<Map<String, Object>> dataList = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>(2);
        Map<String, Object> map2 = new HashMap<>(2);
        map1.put("c21", "新增");
        map2.put("c21", "既往");
        dataList.add(map1);
        dataList.add(map2);

        Stream<Map<String, Object>> mapStream = dataList.stream().map(item -> {
            item.put("c21", "离休");
            return item;
        });

        //[{c21=新增}, {c21=既往}]
        System.out.println(dataList);

        List<Map<String, Object>> mapList = mapStream.collect(Collectors.toList());
        //[{c21=离休}, {c21=离休}]
        System.out.println(mapList);
    }

    /**
     * 提取 List Map 中指定字段的值
     */
    @Test
    @SuppressWarnings("all")
    public void map3() {
        List<Map<String, Object>> dataList = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>(8);
        Map<String, Object> map2 = new HashMap<>(8);
        Map<String, Object> map3 = new HashMap<>(8);

        map1.put("c21", "新增");
        map2.put("c21", "删除");
        map3.put("c21", null);

        map1.put("c22", "c22");
        map2.put("c22", "c22");
        map3.put("c22", "c22");

        dataList.add(map1);
        dataList.add(map2);
        dataList.add(map3);

        System.out.println("提取前：" + dataList);
        List<Object> keySumList = dataList.stream().map(e -> e.get("c21")).collect(Collectors.toList());
        //[新增, 删除, null]
        System.out.println("提取后：" + keySumList);
    }

    /**
     * filter 先过滤，然后使用 map 取值，排除 null 值
     */
    @Test
    public void map4() {
        List<Map<String, Object>> dataList = new ArrayList<>(4);
        Map<String, Object> map1 = new HashMap<>(4);
        Map<String, Object> map2 = new HashMap<>(4);
        Map<String, Object> map3 = new HashMap<>(4);

        map1.put("c21", "既往");
        map2.put("c21", null);
        map3.put("c21", null);

        dataList.add(map1);
        dataList.add(map2);
        dataList.add(map3);

        //提取前=[{c21=既往}, {c21=null}, {c21=null}]
        System.out.println("提取前=" + dataList);
        List<Object> keySumList = dataList.stream().filter(item -> item.get("c21") != null).map(e -> e.get("c21")).collect(Collectors.toList());
        //提取后=[既往]
        System.out.println("提取后=" + keySumList);
    }

    @org.junit.Test
    public void map5() {
        Map<String, Person> personMap = new HashMap<>();
        Person person1 = new Person(1, "张三", DateUtil.parse("1993/08/25"), 7888.89f);
        Person person2 = new Person(2, "李四", DateUtil.parse("1994/08/21"), 5688.71f);
        Person person3 = new Person(3, "王五", DateUtil.parse("1995/08/15"), 8588.53f);
        Person person4 = new Person(4, "马六", DateUtil.parse("1991/06/13"), null);
        Person person5 = new Person(4, "马六", DateUtil.parse("1992/04/05"), 1288.35f);

        personMap.put("001", person1);
        personMap.put("002", person2);
        personMap.put("003", person3);
        personMap.put("004", person4);
        personMap.put("005", person5);

        List<String> collect1 = personMap.values().stream().map(person -> person.getName()).collect(Collectors.toList());
        List<Float> collect2 = personMap.values().stream().map(person -> person.getSalary()).collect(Collectors.toList());

        // [张三, 李四, 王五, 马六, 马六]
        System.out.println(collect1);
        // [7888.89, 5688.71, 8588.53, null, 1288.35]
        System.out.println(collect2);
    }

    /**
     * Stream<T> of(T... values) ：将集合转为有顺序的流，可将多个集合合成一个流
     * 参数可以是集合，数组，基本类型，也可以是任何 POJO 对象
     * collect(Collector<? super T, A, R> collector)：将流转为集合，流中如果有元素是 null，则转换的 list 中对应的也是 null.
     */
    @Test
    public void of1() {
        List<Integer> a = Arrays.asList(1, 2, 3);
        List<Integer> b = Arrays.asList(41, 52, 63);
        List<List<Integer>> collect = Stream.of(a, b).collect(Collectors.toList());
        //[[1, 2, 3], [41, 52, 63]]
        System.out.println(collect);
    }

    /**
     * Stream<T> of(T... values) ：参数可以是集合，数组，基本类型，也可以是任何 POJO 对象
     */
    @Test
    public void of2() {
        Stream<String> stream = Stream.of("覆巢之下", "安有完卵", "天下攘攘", "皆为利往");

        //覆巢之下,安有完卵,天下攘攘,皆为利往,
        stream.forEach(item -> System.out.print(item + ","));
    }

    /**
     * Stream<T> distinct() ：对元素进行去重
     * 它通过使用元素的equals()方法来判断是否重复。
     */
    @Test
    public void distinct1() {
        Stream<String> stream = Stream.of("秦汗", "武汉", "汉武", "武汉", "大楚");
        //秦汗	武汉	汉武	大楚
        stream.distinct().forEach(item -> System.out.print(item + "\t"));
        System.out.println();

        List<Integer> list = Arrays.asList(1, 2, 3, 3, 4, 5, 5);
        List<Integer> collect = list.stream().distinct().collect(Collectors.toList());
        // [1, 2, 3, 4, 5]
        System.out.println(collect);
    }

    /**
     * Stream<T> distinct() ：对元素进行去重
     * 它通过使用元素的equals()方法来判断是否重复。
     */
    @SuppressWarnings("all")
    @Test
    public void distinct2() {
        Map<String, Object> dataMap1 = new HashMap<>();
        Map<String, Object> dataMap2 = new HashMap<>();
        Map<String, Object> dataMap3 = new HashMap<>();
        Map<String, Object> dataMap4 = new HashMap<>();
        Map<String, Object> dataMap5 = new HashMap<>();

        dataMap1.put("init_agency_id", "20792");
        dataMap2.put("init_agency_id", "20793");
        dataMap3.put("init_agency_id", "20794");
        dataMap4.put("init_agency_id", "20792");
        dataMap5.put("init_agency_id", "20795");
        dataMap1.put("agency_code", "002001");
        dataMap2.put("agency_code", "002002");
        dataMap3.put("agency_code", "002003");
        dataMap4.put("agency_code", "002004");
        dataMap5.put("agency_code", "002005");

        List<Map<String, Object>> agencyList = new ArrayList<>();
        agencyList.add(dataMap1);
        agencyList.add(dataMap2);
        agencyList.add(dataMap3);
        agencyList.add(dataMap4);
        agencyList.add(dataMap5);

        List<String> newAgencyList = agencyList.stream().map(pm -> pm.get("init_agency_id").toString()).distinct().collect(Collectors.toList());
        //[20792, 20793, 20794, 20795]
        System.out.println(newAgencyList);
    }

    /**
     * 对 List<Map<String,Object>> 数据去重.
     */
    @Test
    public void testCollectingAndThen() {
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("agency_id", "20791");
        map.put("agency_name", "单位1");
        map.put("agency_codee", "001001");
        list.add(new HashMap<>(map));
        map.put("agency_id", "20792");
        map.put("agency_name", "单位2");
        map.put("agency_codee", "001002");
        list.add(new HashMap<>(map));
        map.put("agency_id", "20793");
        map.put("agency_name", "单位3");
        map.put("agency_codee", "001003");
        list.add(new HashMap<>(map));
        map.put("agency_id", "20792");
        map.put("agency_name", "单位21");
        map.put("agency_codee", "001002");
        list.add(new HashMap<>(map));

        // {agency_codee=001001, agency_id=20791, agency_name=单位1},
        // {agency_codee=001002, agency_id=20792, agency_name=单位2},
        // {agency_codee=001003, agency_id=20793, agency_name=单位3},
        // {agency_codee=001002, agency_id=20792, agency_name=单位21}
        System.out.println("去重前：" + list);

        // 根据单位ID进行去重，如果单位ID为null，则直接会被过滤掉.
        list = list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                new TreeSet<>(Comparator.comparing((o) -> o.get("agency_id")))), ArrayList::new));

        // {agency_codee=001001, agency_id=20791, agency_name=单位1},
        // {agency_codee=001002, agency_id=20792, agency_name=单位2},
        // {agency_codee=001003, agency_id=20793, agency_name=单位3}
        System.out.println("去重后：" + list);
    }

    /**
     * Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper) 将多个集合中的元素合并成一个集合
     */
    @Test
    public void flatMap1() {
        List<Integer> a = Arrays.asList(1, 2, 3);
        List<Integer> b = Arrays.asList(41, 51, 61);
        Stream<Integer> stream = Stream.of(a, b).flatMap(list -> list.stream());
        //2 4 6 82 102 122
        stream.forEach(item -> {
            item = 2 * item;
            System.out.print(item + " ");
        });
    }

    /**
     * 将两个字符数组合并成一个新的字符数组。
     */
    @Test
    public void flatMap2() {
        List<String> list = Arrays.asList("m,k,l,a", "1,3,5,7");
        List<String> listNew = list.stream().flatMap(s -> {
            // 将每个元素转换成一个stream
            String[] split = s.split(",");
            Stream<String> s2 = Arrays.stream(split);
            return s2;
        }).collect(Collectors.toList());

        //处理前的集合：[m,k,l,a, 1,3,5,7]
        System.out.println("处理前的集合：" + list);
        //处理后的集合：[m, k, l, a, 1, 3, 5, 7]
        System.out.println("处理后的集合：" + listNew);
    }

    @Test
    public void flatMap3() {
        List<String> list1 = Arrays.asList("Java", "Python", "C++");
        List<String> list2 = Arrays.asList("JavaScript", "Ruby");

        List<String> mergedList = Stream.of(list1, list2).flatMap(Collection::stream).collect(Collectors.toList());

        // 输出结果为 [Java, Python, C++, JavaScript, Ruby]
        System.out.println(mergedList);
    }

    /**
     * Stream<T> sorted(Comparator<? super T> comparator) :排序操作
     * {@link Comparator} 用于比较的函数
     * naturalOrder 顺序排序
     * reverseOrder 倒序排序
     * <p>
     * 更多排序请参考 https://gitee.com/apache-kafka/blob/master/src/test/java/com/wmx/apachekafka/jdk/ComparatorTest.java
     */
    @Test
    public void sorted1() {
        // 元素中不能有 null，否则 NullPointerException
        List<String> list = Arrays.asList("c", "e", null, "a", null, "d", "b");

        //源列表：[c, e, null, a, null, d, b]
        System.out.println("源列表：" + list);

        List<String> collect1 = list.stream().sorted(Comparator.nullsLast(Comparator.naturalOrder())).collect(Collectors.toList());
        //顺序：[a, b, c, d, e, null, null]
        System.out.println("顺序：" + collect1);

        List<String> collect2 = list.stream().sorted(Comparator.nullsLast(Comparator.reverseOrder())).collect(Collectors.toList());
        //倒序：[e, d, c, b, a, null, null]
        System.out.println("倒序：" + collect2);
    }

    /**
     * Stream<T> sorted(Comparator<? super T> comparator) :排序操作
     * {@link Comparator} 用于比较的函数，naturalOrder 顺序排序，reverseOrder 倒序排序
     * https://wangmaoxiong.blog.csdn.net/article/details/106885872#Comparator%20%E6%8E%92%E5%BA%8F%E6%AF%94%E8%BE%83%E5%99%A8
     * <p>
     * 更多排序请参考 https://gitee.com/apache-kafka/blob/master/src/test/java/com/wmx/apachekafka/jdk/ComparatorTest.java
     */
    @Test
    public void sorted2() {
        Map<String, Object> dataMap1 = new LinkedHashMap<>();
        Map<String, Object> dataMap2 = new LinkedHashMap<>();
        Map<String, Object> dataMap3 = new LinkedHashMap<>();
        Map<String, Object> dataMap4 = new LinkedHashMap<>();
        Map<String, Object> dataMap5 = new LinkedHashMap<>();

        dataMap1.put("column_name", "agency_id");
        dataMap2.put("column_name", "work_sub");
        dataMap3.put("column_name", null);
        dataMap4.put("column_name", "biz_key");
        dataMap5.put("column_name", "grad_sala");

        List<Map<String, Object>> colsList = new ArrayList<>();
        colsList.add(dataMap1);
        colsList.add(dataMap2);
        colsList.add(dataMap3);
        colsList.add(dataMap4);
        colsList.add(dataMap5);

        //排序前：[{column_name=agency_id}, {column_name=work_sub}, {column_name=null}, {column_name=biz_key}, {column_name=grad_sala}]
        System.out.println("排序前：" + colsList);
        // 按字母自然顺序排序，Comparator.nullsLast 只是处理List中的null元素，而不是map中的null元素
        colsList = colsList.stream().sorted(Comparator.nullsLast(Comparator.comparing(item -> String.valueOf(item.get("column_name"))))).collect(Collectors.toList());
        //排序后：[{column_name=agency_id}, {column_name=biz_key}, {column_name=grad_sala}, {column_name=null}, {column_name=work_sub}]
        System.out.println("排序后：" + colsList);
    }

    @Test
    public void sorted3() {
        Date date = new Date();
        Person person1 = new Person(1, "张三", date, 7888.89f);
        Person person2 = new Person(2, "李四", date, 5688.71f);
        Person person3 = new Person(3, "王五", date, 8588.53f);
        Person person4 = new Person(4, "马六", date, null);
        Person person5 = new Person(4, "马六", date, 4512F);
        List<Person> persons = ListUtil.toList(person1, person2, person3, person4, person5);

        // 按工资从小到大排序，null值排在末尾。
        List<Person> sortedPersons = persons.stream().sorted(Comparator.comparingDouble(item -> item.getSalary() == null ? Double.MAX_VALUE : item.getSalary())).collect(Collectors.toList());

        // Person{id=4, name='马六', birthday=Sat Nov 25 16:50:28 CST 2023, salry=4512.0}
        // Person{id=2, name='李四', birthday=Sat Nov 25 16:50:28 CST 2023, salry=5688.71}
        // Person{id=1, name='张三', birthday=Sat Nov 25 16:50:28 CST 2023, salry=7888.89}
        // Person{id=3, name='王五', birthday=Sat Nov 25 16:50:28 CST 2023, salry=8588.53}
        // Person{id=4, name='马六', birthday=Sat Nov 25 16:50:28 CST 2023, salry=null}
        sortedPersons.stream().forEach(item -> System.out.println(item));
    }

    @Test
    public void sortedMap1() {
        Map<String, Person> personMap = new HashMap<>();
        Person person1 = new Person(1, "张三", DateUtil.parse("1993/08/25"), 7888.89f);
        Person person2 = new Person(2, "李四", DateUtil.parse("1994/08/21"), -5688.71f);
        Person person3 = new Person(3, "王五", DateUtil.parse("1995/08/15"), 8588.53f);
        Person person4 = new Person(4, "马六", DateUtil.parse("1991/06/13"), null);
        Person person5 = new Person(4, "马六", DateUtil.parse("1992/04/05"), 1288.35f);

        personMap.put("001", person1);
        personMap.put("002", person2);
        personMap.put("003", person3);
        personMap.put("004", person4);
        personMap.put("005", person5);

        // 按工资由大到小排序输出；null值当作0处理；使用 -1 相乘达到倒序效果;
        Map<String, Person> sortedMap = personMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.comparingDouble(person -> person.getSalary() == null ? 0 : -1 * person.getSalary())))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, // 在键冲突时保留旧值
                        LinkedHashMap::new)); // 使用LinkedHashMap保持顺序
        // {003=Person{id=3, name='王五', birthday=1995-08-15 00:00:00, salary=8588.53},
        // 001=Person{id=1, name='张三', birthday=1993-08-25 00:00:00, salary=7888.89},
        // 005=Person{id=4, name='马六', birthday=1992-04-05 00:00:00, salary=1288.35},
        // 004=Person{id=4, name='马六', birthday=1991-06-13 00:00:00, salary=null},
        // 002=Person{id=2, name='李四', birthday=1994-08-21 00:00:00, salary=-5688.71}
        // }
        System.out.println(sortedMap);
    }

    @Test
    public void sortedMap2() {
        Map<String, Person> personMap = new HashMap<>();
        Person person1 = new Person(1, "张三", DateUtil.parse("1993/08/25"), 7888.89f);
        Person person2 = new Person(2, "李四", DateUtil.parse("1994/08/21"), -5688.71f);
        Person person3 = new Person(3, "王五", DateUtil.parse("1995/08/15"), 8588.53f);
        Person person4 = new Person(4, "马六", DateUtil.parse("1991/06/13"), null);
        Person person5 = new Person(4, "马六", DateUtil.parse("1992/04/05"), 1288.35f);

        personMap.put("001", person1);
        personMap.put("002", person2);
        personMap.put("003", person3);
        personMap.put("004", person4);
        personMap.put("005", person5);

        // 按工资由大到小排序输出
        Map<String, Person> sortedMap = personMap.entrySet().stream()
                .filter(entry -> entry.getValue().getSalary() != null)
                .sorted(Map.Entry.comparingByValue(Comparator.comparingDouble(Person::getSalary).reversed()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, // 在键冲突时保留旧值
                        LinkedHashMap::new)); // 使用LinkedHashMap保持顺序
        // {
        // 003=Person{id=3, name='王五', birthday=1995-08-15 00:00:00, salary=8588.53},
        // 001=Person{id=1, name='张三', birthday=1993-08-25 00:00:00, salary=7888.89},
        // 005=Person{id=4, name='马六', birthday=1992-04-05 00:00:00, salary=1288.35},
        // 002=Person{id=2, name='李四', birthday=1994-08-21 00:00:00, salary=-5688.71}
        // }
        System.out.println(sortedMap);
    }

    /**
     * long count() :统计流中元素个数
     */
    @Test
    public void count() {
        Stream<String> stream = Stream.of("c", "e", "a", "d", "b", null);
        long count = stream.count();
        // 6
        System.out.println(count);

        // 97
        System.out.println((byte) "a".charAt(0));

        // 统计大于 c 的字符个数
        long count1 = Stream.of("c", "e", "a", "d", "b").filter(item -> item.charAt(0) > 99).count();
        // 2
        System.out.println(count1);
    }

    /**
     * Optional<T> min(Comparator<? super T> comparator) ：获取元素中的最小值
     */
    @Test
    public void min1() {
        List<Integer> list = Arrays.asList(31, 22, 133, 465, 125);
        // Optional<T> min(Comparator<? super T> comparator);
        Optional<Integer> optional = list.stream().min(Comparator.naturalOrder());
        Optional<Integer> optional2 = list.stream().max(Comparator.naturalOrder());
        Integer value1 = optional.get();
        Integer value2 = optional2.get();
        //22	465
        System.out.println(value1 + "\t" + value2);

        Optional<Integer> integer = list.stream().max(Comparator.comparing(item -> item));
        //465
        System.out.println(integer.get());

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        // 获取列表中的最小数字
        Optional<Integer> minNumber = numbers.stream().min(Integer::compareTo);
        // 输出结果为 Optional[1]
        System.out.println(minNumber);
    }

    /**
     * Optional<T> max(Comparator<? super T> comparator) ：获取元素中的最大值
     */
    @Test
    public void max1() {
        List<Integer> list = Arrays.asList(31, 22, 133, 465, 125, null);
        Optional<Integer> optional2 = list.stream().filter(item -> item != null).max(Comparator.naturalOrder());
        Integer value2 = optional2.get();
        // 465
        System.out.println(value2);

        Optional<Integer> integer = list.stream().filter(item -> item != null).max(Comparator.comparing(item -> item));
        // 465
        System.out.println(integer.get());

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        // 获取列表中的最大数字
        Optional<Integer> maxNumber = numbers.stream().max(Integer::compareTo);
        // 输出结果为 Optional[5]
        System.out.println(maxNumber);
    }

    /**
     * 获取最长的元素
     */
    @Test
    public void max2() {
        List<String> list = Arrays.asList("你好", "的话就是", "但还是觉得和", "的就是看电视", null, "贷记卡圣诞节ask的");

        Optional<String> max = list.stream().filter(item -> item != null).max(Comparator.comparing(item -> item.length()));
        // 最长的字符串：贷记卡圣诞节ask的
        System.out.println("最长的字符串：" + max.get());
    }

    @Test
    public void max3() {
        Date date = new Date();
        Person person1 = new Person(1, "张三", date, 7888.89f);
        Person person2 = new Person(2, "李四", date, 5688.71f);
        Person person3 = new Person(3, "王五", date, 8588.53f);
        Person person4 = new Person(4, "马六", date, null);
        Person person5 = new Person(4, "马六", date, 4512F);
        List<Person> persons = ListUtil.toList(person1, person2, person3, person4, person5);
        // 查询工资最高的员工
        Optional<Person> max = persons.stream().filter(item -> item.getSalary() != null).max(Comparator.comparing(item -> item.getSalary()));
        if (max.isPresent()) {
            // 工资最高的人是：Person{id=3, name='王五', birthday=Sat Nov 25 17:13:25 CST 2023, salry=8588.53}
            System.out.println("工资最高的人是：" + max.get().toString());
        }
    }

    /**
     * Stream<T> skip(long n) : 表示跳过 n 个元素，单 n 超过实际个数时，返回空的流
     */
    @Test
    public void skip1() {
        List<String> list = Arrays.asList("a", "b", "c", "d", "e", "f", "g");
        // c	d	e	f	g
        list.stream().skip(2).forEach(item -> System.out.print(item + "\t"));
        System.out.println();

        // f	g
        list.stream().skip(5).forEach(item -> System.out.print(item + "\t"));
        System.out.println();

        //
        list.stream().skip(50).forEach(item -> System.out.print(item + "\t"));
    }

    /**
     * Stream<T> limit(long maxSize): 表示截取 maxSize 个元素，当 maxSize 超过实际个数时，则最多截取实际长度
     */
    @Test
    public void limit1() {
        List<String> list = Arrays.asList("a", "b", "c", "d", "e", "f", "g");
        // a	b	c
        list.stream().limit(3).forEach(item -> System.out.print(item + "\t"));
        System.out.println();

        // f	g
        list.stream().skip(5).limit(10).forEach(item -> System.out.print(item + "\t"));
    }

    /**
     * Stream<T> concat(Stream<? extends T> a, Stream<? extends T> b): 拼接两个流为一个流
     */
    @Test
    public void concat() {
        List<String> list = Arrays.asList("a", "b");
        List<Integer> list2 = Arrays.asList(110, 120);
        Stream<Object> concatStream = Stream.concat(list.stream(), list2.stream());
        //a b 110 120
        concatStream.forEach(item -> System.out.print(item + " "));
        System.out.println();

        List<String> list11 = Arrays.asList("apple", "banana", "cherry");
        List<String> list22 = Arrays.asList("orange", "pineapple", "mango");
        List<String> collect = Stream.concat(list11.stream(), list22.stream()).filter(s -> s.length() > 5).collect(Collectors.toList());
        // [banana, cherry, orange, pineapple]
        System.out.println(collect);
    }

    /**
     * Stream<E> parallelStream() 可以并行计算，速度比 stream 更快
     * boolean anyMatch(Predicate<? super T> predicate): 只要流中的任意一个元素满足条件，则返回 true
     */
    @Test
    public void anyMatch1() {
        List<String> list = Arrays.asList("长沙", "长安", "常州", "昌平");
        boolean result1 = list.parallelStream().anyMatch(item -> item.equals("长安"));
        boolean result2 = list.parallelStream().anyMatch(item -> item.equals("西安") || item.contains("安"));
        //true	true
        System.out.println(result1 + "\t" + result2);
    }

    @Test
    public void anyMatch2() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        // 判断列表中是否有大于5的数字
        boolean hasNumberGreaterThan5 = numbers.stream().anyMatch(n -> n > 5);
        // 输出结果为 false
        System.out.println(hasNumberGreaterThan5);
    }

    /**
     * boolean allMatch(Predicate<? super T> predicate): 流中的元素全部满足条件时，返回 true
     */
    @Test
    public void allMatch1() {
        List<Integer> list = Arrays.asList(22, 34, 55, 43, 28);
        boolean allMatch1 = list.stream().allMatch(item -> item >= 18);
        boolean allMatch2 = list.parallelStream().allMatch(item -> item >= 28);
        //true,false
        System.out.println(allMatch1 + "," + allMatch2);
    }

    @Test
    public void allMatch2() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        // 判断列表中是否所有数字都小于10：
        boolean allNumbersLessThan10 = numbers.stream().allMatch(n -> n < 10);
        // 输出结果为 true
        System.out.println(allNumbersLessThan10);
    }

    /**
     * 判断集合中是否没有任何一个元素符合指定条件。
     */
    @Test
    public void noneMatchTest1() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        // 判断列表中是否没有负数
        boolean allNumbersLessThan10 = numbers.stream().noneMatch(n -> n < 0);
        // 输出结果为 true
        System.out.println(allNumbersLessThan10);
    }

    /**
     * Stream API 提供数字流 包括 IntStream、DoubleStream、LongStream
     * 1、int sum() : 对流中的元素进行求和，底层也是使用 reduce(0, Integer::sum) 方法
     */
    @Test
    public void sum() {
        IntStream intNumbers = IntStream.of(1, 4, 2, 3, 5, 5);
        //20
        int sum = intNumbers.sum();
        System.out.println(sum);
    }

    /**
     * DoubleStream mapToDouble(ToDoubleFunction<? super T> mapper)：将对象流转换为 DoubleStream
     * 1、{@link ToDoubleFunction} 接口中有一个 double applyAsDouble(T value)，函数式接口接收一个参数，并将处理结果以  double 类型返回
     */
    @Test
    public void mapToDouble() {
        //对 double 类型列表中的所有元素进行求和
        List<Double> doubleList = Arrays.asList(223.582, 52.426, 113.532);
        double result = doubleList.stream().mapToDouble(aDouble -> aDouble.doubleValue()).sum();
        //389.53999999999996
        System.out.println(result);

        //格式化数字，保留小数点后四位
        NumberFormat numberFormat = NumberFormat.getInstance();
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.applyPattern("#,###.0000");
        String format = decimalFormat.format(result);
        //389.5400
        System.out.println(format);
    }

    /**
     * 归约(reduce):是把一个流缩减成一个值，能实现对集合求和、求乘积和求最值操作。
     * <p>
     * Optional<T> reduce(BinaryOperator<T> accumulator)
     * 1、函数式接口 BinaryOperator，继承于 BiFunction，其中有一个 R apply(T t, U u) 方法
     * 2、apply 方法中的参数 t 表示当前计算的值，u 表示下一个元素。
     * 3、返回的值 r 会作为参数 t 继续传入，非常适合累加、累乘等等操作
     * 4、boolean isPresent()：如果存在值，则返回 true，否则返回 false.
     * 5、如果 {@code Optional} 中有值，则返回该值，否则抛出异常。
     */
    @Test
    public void reduce1() {
        List<Integer> integerList = Arrays.asList(1, 2, 3, 5, 6, 7, 8, 9, 10);
        Optional<Integer> reduce = integerList.stream().reduce((total, item) -> {
            total += item;
            return total;
        });
        boolean present = reduce.isPresent();
        if (present) {
            //1-10累加结果=51
            System.out.println("1-10累加=" + reduce.get());
        }

        Integer integer = integerList.stream().reduce((x, y) -> x * y).get();
        //求乘积：907200
        System.out.println("求乘积：" + integer);

        Integer integer1 = integerList.stream().reduce((x, y) -> x > y ? x : y).get();
        //求最大值：10
        System.out.println("求最大值：" + integer1);
    }

    /**
     * T reduce(T identity, BinaryOperator<T> accumulator)
     * * identity – 在 reduce(BinaryOperator<T> accumulator) 的基础上提供一个初始值
     * * accumulator – 与 reduce(BinaryOperator<T> accumulator) 一致
     */
    @Test
    public void reduce2() {
        Stream<Integer> intNumbers = Stream.of(5, 1, 100);
        int result = intNumbers.reduce(10, (a, b) -> Integer.sum(a, b));
        //116
        System.out.println("累加：" + result);

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        Integer reduce = list.stream().reduce(0, Integer::sum);
        // 15
        System.out.println("1-5求和=" + reduce);
    }

    @Test
    public void reduce3() {
        Stream<BigDecimal> bigDecimalNumber = Stream.of(BigDecimal.valueOf(100), BigDecimal.valueOf(210));
        BigDecimal result = bigDecimalNumber.reduce(BigDecimal.ZERO, (total, item) -> total.add(item));
        //BigDecimal 类型流式求和：310
        System.out.println("BigDecimal 类型流式求和：" + result);
    }

    /**
     * peek 操作用于对Stream流中的元素进行中间操作，不改变元素本身。
     * 它通过传递一个Consumer接口的Lambda表达式作为参数来实现中间操作。
     */
    @Test
    public void peekTest1() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        // 使用peek操作在每个数字被处理时输出一条日志
        List<Integer> collect = numbers.stream().peek(n -> System.out.println("Processing number: " + n)).map(n -> n * 2).collect(Collectors.toList());
        // Processing number: 1
        // Processing number: 2
        // Processing number: 3
        // Processing number: 4
        // Processing number: 5
    }


    private List<Map<String, Object>> getDataList() {
        List<Map<String, Object>> dataList = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>(8);
        Map<String, Object> map2 = new HashMap<>(8);
        Map<String, Object> map3 = new HashMap<>(8);
        Map<String, Object> map4 = new HashMap<>(8);

        map1.put("c21", "新增");
        map1.put("agency_code", "201025");
        map1.put("address", "深圳市");

        map2.put("c21", "既往");
        map2.put("agency_code", "002015");
        map2.put("address", "长沙市");

        map3.put("c21", "删除");
        map3.put("agency_code", "304100");
        map3.put("address", "武汉市");

        map4.put("c21", "既往");
        map4.put("agency_code", "324100");
        map4.put("address", "深圳市");

        dataList.add(map1);
        dataList.add(map2);
        dataList.add(map3);
        dataList.add(map4);

        return dataList;
    }

}
