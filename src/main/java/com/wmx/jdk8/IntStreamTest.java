package com.wmx.jdk8;

import cn.hutool.core.lang.Console;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * IntStream 流
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/6/22 19:38
 */
public class IntStreamTest {

    /**
     * static IntStream of(int... values) 返回元素为指定值的有序流。
     */
    @Test
    public void testOf() {
        IntStream intStream = IntStream.of(1, 2, 3, 4, 4, 5, 6, 7, 8, 9, 9, 0);
        int[] ints = intStream.toArray();
        // [1, 2, 3, 4, 4, 5, 6, 7, 8, 9, 9, 0]
        Console.log(ints);
    }

    /**
     * IntStream mapToInt(ToIntFunction<? super T> mapper)：Stream 转 IntStream
     * Stream<Integer> boxed()：对原生装箱，即将基本类型转为包装类型
     */
    @Test
    public void testMapToInt() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0, max = 10; i < max; i++) {
            list.add(i);
        }
        // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        System.out.println(list);
        List<Integer> collect = list.stream().mapToInt(item -> item).map(item -> ++item).boxed().collect(Collectors.toList());
        // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        System.out.println(collect);
    }

    /**
     * int sum() 返回该流中元素的总和
     * OptionalInt max() 返回描述此流的最大元素的｛@code OptionalInt｝，如果此流为空，则返回空的可选项。
     * OptionalInt min() 返回描述此流的最小元素的｛@code OptionalInt｝，如果此流为空，则返回空的可选项。
     * long count() 返回该流中的元素数
     */
    @Test
    public void testSum() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int sum = Arrays.stream(array).sum();
        // 55
        System.out.println(sum);

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, null);
        int sum1 = list.stream().filter(item -> item != null).mapToInt(item -> item).sum();
        // 55
        System.out.println(sum1);

        List<Integer> list2 = Arrays.asList(1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, null);
        // 10
        OptionalInt max = list2.stream().filter(item -> item != null).mapToInt(item -> item).max();
        System.out.println(max.isPresent() ? max.getAsInt() : null);

        // 1
        OptionalInt min = list2.stream().filter(item -> item != null).mapToInt(item -> item).min();
        System.out.println(min.isPresent() ? min.getAsInt() : null);

        // 13
        System.out.println(list2.stream().filter(item -> item != null).mapToInt(item -> item).count());
    }

    /**
     * IntStream distinct() 对流中的原生去重，null 元素会导致 NullPointerException
     * void forEach(IntConsumer action) 对此流的每个元素执行一个操作
     */
    @Test
    public void testDistinct() {
        List<Integer> list2 = Arrays.asList(1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, null);
        List<Integer> collect = list2.stream().filter(item -> item != null).mapToInt(item -> item).distinct().boxed().collect(Collectors.toList());
        // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        System.out.println(collect);

        // 1 1 2 3 4 5 6 7 8 9 10 10 10
        list2.stream().filter(item -> item != null).mapToInt(item -> item).forEach(item -> System.out.print(item + " "));
    }


}
