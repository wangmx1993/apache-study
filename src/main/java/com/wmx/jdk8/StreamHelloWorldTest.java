package com.wmx.jdk8;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Stream 将要处理的元素集合看作一种流，在流的过程中，借助Stream API对流中的元素进行操作，比如：筛选、排序、聚合等
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/5 15:00
 */
public class StreamHelloWorldTest {

    /**
     * 通过 java.util.Collection.stream() 方法用集合创建流
     */
    @Test
    public void testStream1() {
        List<String> list = Arrays.asList("a", "b", "c");
        // 创建一个顺序流
        Stream<String> stream = list.stream();
        //3
        System.out.println(stream.count());

        // 创建一个并行流
        Stream<String> parallelStream = list.parallelStream();
        //Optional[a]
        System.out.println(parallelStream.findFirst());
    }

    /**
     * 使用java.util.Arrays.stream(T[] array)方法用数组创建流
     */
    @Test
    public void testStream2() {
        int[] array = {1, 3, 5, 6, 8};
        IntStream stream = Arrays.stream(array);
        //23
        System.out.println(stream.sum());
    }

    /**
     * 使用Stream的静态方法：of()、iterate()、generate()
     * <p>
     * Stream<T> iterate(final T seed, final UnaryOperator<T> f): 迭代产生一个无限且有序的流。seed 是初始种子，f 是下一个元素的产生方式。
     * Stream<T> limit(long maxSize):截取 maxSize 个元素，当 maxSize 超过实际个数时，则最多截取实际长度
     * <p>
     * Stream<T> generate(Supplier<T> s):创建一个无限且无序的流，元素由提供的 Supplier 生成。适用于生成恒定流、随机元素流等。
     */
    @Test
    public void testStream3() {
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6);
        stream.forEach(item -> System.out.println(item));

        Stream<Integer> stream2 = Stream.iterate(0, (x) -> x + 3).limit(4);
        stream2.forEach(System.out::println);

        Stream<Double> stream3 = Stream.generate(Math::random).limit(3);
        stream3.forEach(System.out::println);
    }

    /**
     * Builder<T> builder(): 创建构建器，用于构建 java.util.stream.Stream
     * Builder<T> add(T t): 向正在生成的流中添加元素。
     * Stream<T> build(): 生成流
     * collect(Collector<? super T, A, R> collector): 将流转为集合
     */
    @Test
    public void testStream4() {
        Stream<Object> stream = Stream.builder().add("大秦").add("大商").add("大魏").build();
        List<Object> objectList = stream.collect(Collectors.toList());
        //[大秦, 大商, 大魏]
        System.out.println(objectList);
    }

    /**
     * 除了直接创建并行流，还可以通过parallel()把顺序流转换成并行流：
     */
    @Test
    public void testStream5() {
        Stream<Object> stream = Stream.builder().add("大秦").add("大商").add("大魏").build();
        long count = stream.parallel().count();
        System.out.println(count);

    }


}
